package com.aralpilipinas.app.server.request;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aralpilipinas.app.config.Keys;
import com.aralpilipinas.app.config.Url;
import com.aralpilipinas.app.data.model.api.SampleModel;
import com.aralpilipinas.app.data.model.api.SchoolModel;
import com.aralpilipinas.app.data.model.api.UserModel;
import com.aralpilipinas.app.data.preference.UserData;
import com.aralpilipinas.app.vendor.server.request.APIRequest;
import com.aralpilipinas.app.vendor.server.request.APIResponse;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;
import com.aralpilipinas.app.vendor.server.transformer.CollectionTransformer;
import com.aralpilipinas.app.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Auth {

    public static Auth getDefault(){
        return new Auth();
    }

    public void loginStudent(Context context, String email, String password) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getLoginStudent(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginStudentResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.EMAIL, email)
                .addParameter(Keys.PASSWORD, password)
                .showDefaultProgressDialog("Logging in...")
                .execute();

    }
    public void loginTeacher(Context context, String email, String password) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getLoginTeacher(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginTeacherResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.EMAIL, email)
                .addParameter(Keys.PASSWORD, password)
                .showDefaultProgressDialog("Logging in...")
                .execute();

    }

    public void loginViaPinStudent(Context context, String email, String pin) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.loginStudentViaPin(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginStudentResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.EMAIL, email)
                .addParameter(Keys.PIN, pin)
                .showDefaultProgressDialog("Logging in...")
                .execute();

    }
    public void loginViaPinTeacher(Context context, String email, String pin) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.loginTeacherViaPin(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginStudentResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.EMAIL, email)
                .addParameter(Keys.PIN, pin)
                .showDefaultProgressDialog("Logging in...")
                .execute();

    }

    public APIRequest preRegStudent(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getStudentPrereg(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PreRegister(this));
            }
        };

        apiRequest
                .showDefaultProgressDialog("Validating...");

        return apiRequest;
    }
    public APIRequest preRegTeacher(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getTeacherPrereg(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new PreRegister(this));
            }
        };

        apiRequest
                .showDefaultProgressDialog("Validating...");

        return apiRequest;
    }

    public static void logout(int id) {

    }

    public void signup(SampleModel userModel) {

    }

    public static void facebook(SampleModel userModel) {

    }

    public APIRequest register(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getRegisterStudent(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RegisterResponse(this));
            }
        };

        apiRequest
                .showDefaultProgressDialog("Registering...");

        return apiRequest;
    }
    public APIRequest registerTeacher(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getRegisterTeacher(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RegisterResponse(this));
            }
        };

        apiRequest
                .showDefaultProgressDialog("Registering...");

        return apiRequest;
    }
    public void logoutStudent(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getLogoutStudent(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LogoutResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Logging out...")
                .execute();
    }
    public void logoutTeacher(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getLogoutTeacher(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LogoutResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Logging out...")
                .execute();
    }
    public void checkStudentLogin(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getCheckStudLogin(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CheckStudentResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();

    }
    public void checkTeacherLogin(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getCheckTeachLogin(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CheckTeacherResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
    }
    public void refreshStudentToken(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getStudentToken(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RefreshStudentTokenResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();

    }
    public void refreshTeacherToken(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getTeacherToken(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RefreshTeacherTokenResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();

    }
    public void updateTeacherAvatar(Context context, File file) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getTeacherAvatar(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdateAvatarResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.FILE, file)
                .showDefaultProgressDialog("Updating Avatar...")
                .execute();

    }
    public void updateStudentAvatar(Context context, File file) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getStudentAvatar(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdateAvatarResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.FILE, file)
                .showDefaultProgressDialog("Updating Avatar...")
                .execute();

    }

    public APIRequest updateTeacherProfile(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.editTeacherProfile(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdateProfileResponse(this));
            }
        };
        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Saving Changes...");
        return apiRequest;


    }
    public APIRequest updateStudentProfile(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.editStudentProfile(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdateProfileResponse(this));
            }
        };
        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Saving Changes...");
        return apiRequest;


    }

    public void studentPass(Context context, String currentPass, String password, String passConfirm) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.updateStudentPassword(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdatePasswordResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.PASSWORD, password)
                .addParameter(Keys.CURRENT_PASSWORD, currentPass)
                .addParameter(Keys.PASSWORD_CONFIRMATION, passConfirm)
                .showDefaultProgressDialog("Updating password...")
                .execute();

    }
    public void teacherPass(Context context, String currentPass, String password, String passConfirm) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.updateTeacherPassword(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdatePasswordResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.PASSWORD, password)
                .addParameter(Keys.CURRENT_PASSWORD, currentPass)
                .addParameter(Keys.PASSWORD_CONFIRMATION, passConfirm)
                .showDefaultProgressDialog("Updating password...")
                .execute();

    }
    public void teacherProfile(Context context){
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.teacherProfileDetails(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new TeacherProfileResponse(this));
            }
        };
        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Loading . . .")
                .execute();
    }
    public void studentProfile(Context context){
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.studentProfileDetails(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new StudentProfileResponse(this));
            }
        };
        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Loading . . .")
                .execute();
    }

    public void getRequestAll (Context context) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<SchoolModel>>(context) {
            @Override
            public Call<CollectionTransformer<SchoolModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestList(Url.getListSchool (),  getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RequestSchoolResponse(this));
            }
        };
       apiRequest
               .showDefaultProgressDialog("Loading . . .")
               .execute();
    }




    public interface RequestService {
            @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserModel>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<SchoolModel>> requestSingleList(@Path(value = "p", encoded = true) String p, @Part List<MultipartBody.Part> part);



        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<SchoolModel>> requestList(@Path(value = "p", encoded = true) String p,  @Part List<MultipartBody.Part> part);


    }


    public class LoginStudentResponse extends APIResponse<SingleTransformer<UserModel>> {
        public LoginStudentResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class LoginTeacherResponse extends APIResponse<SingleTransformer<UserModel>> {
        public LoginTeacherResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class RegisterResponse extends APIResponse<BaseTransformer> {
        public RegisterResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class LogoutResponse extends APIResponse<BaseTransformer> {
        public LogoutResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class LoginResponse extends APIResponse<SingleTransformer<UserModel>> {
        public LoginResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class PreRegister extends APIResponse<BaseTransformer> {
        public PreRegister(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class CheckStudentResponse extends APIResponse<SingleTransformer<UserModel>> {
        public CheckStudentResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class CheckTeacherResponse extends APIResponse<SingleTransformer<UserModel>> {
        public CheckTeacherResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class RefreshStudentTokenResponse extends APIResponse<SingleTransformer<UserModel>> {
        public RefreshStudentTokenResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class RefreshTeacherTokenResponse extends APIResponse<SingleTransformer<UserModel>> {
        public RefreshTeacherTokenResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class UpdateAvatarResponse extends APIResponse<BaseTransformer> {
        public UpdateAvatarResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class UpdateProfileResponse extends APIResponse<SingleTransformer<UserModel>> {
        public UpdateProfileResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class UpdatePasswordResponse extends APIResponse<BaseTransformer> {
        public UpdatePasswordResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class TeacherProfileResponse extends APIResponse<SingleTransformer<UserModel>> {
        public TeacherProfileResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class StudentProfileResponse extends APIResponse<SingleTransformer<UserModel>> {
        public StudentProfileResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class RequestSchoolResponse extends APIResponse<CollectionTransformer<SchoolModel>> {
        public RequestSchoolResponse (APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
