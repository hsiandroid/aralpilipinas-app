package com.aralpilipinas.app.server.request;

import android.content.Context;

import com.aralpilipinas.app.config.Url;
import com.aralpilipinas.app.data.model.api.UserModel;
import com.aralpilipinas.app.data.preference.UserData;
import com.aralpilipinas.app.vendor.server.request.APIRequest;
import com.aralpilipinas.app.vendor.server.request.APIResponse;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;
import com.aralpilipinas.app.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class VPN {

    public static VPN getDefault() {
        return new VPN();
    }



    public void teachersCredsVPN(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingletransformer(Url.getTeacherVPNCreds(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new TeacherVPNResponse(this));
            }
        };
        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
    }
    public void studentsCredsVPN(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingletransformer(Url.getStudentVPNCreds(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new StudentVPNResponse(this));
            }
        };
        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserModel>> requestSingletransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);
    }

    public class TeacherVPNResponse extends APIResponse<SingleTransformer<UserModel>> {
        public TeacherVPNResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class StudentVPNResponse extends APIResponse<SingleTransformer<UserModel>> {
        public StudentVPNResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
