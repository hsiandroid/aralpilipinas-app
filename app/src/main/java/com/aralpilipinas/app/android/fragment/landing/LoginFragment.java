package com.aralpilipinas.app.android.fragment.landing;

import android.database.Cursor;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LandingActivity;
import com.aralpilipinas.app.android.activity.LoginActivity;
import com.aralpilipinas.app.android.fragment.sqlite.DBManager;
import com.aralpilipinas.app.data.model.api.HandlerModel;
import com.aralpilipinas.app.data.model.api.UserModel;
import com.aralpilipinas.app.data.preference.UserData;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.android.java.ToastMessage;
import com.aralpilipinas.app.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LoginFragment extends BaseFragment implements AdapterView.OnItemSelectedListener {
    public static final String TAG = LoginFragment.class.getName().toString();

    private LoginActivity activity;
    @BindView(R.id.emailET)             EditText emailET;
    @BindView(R.id.passET)              EditText passET;
    @BindView(R.id.spinner) Spinner spinner;
    String selected;
    String user;
    private DBManager dbManager;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewReady() {
        activity = (LoginActivity) getContext();
        spinner.setOnItemSelectedListener(this);
        dbManager = new DBManager(activity);
        dbManager.open();

    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginStudentResponse loginResponse){
        try{
            SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
            if (singleTransformer.status){
                    UserData.insertStudentDetails(singleTransformer.data);
                    UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
                    ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
                    activity.startMainActivity("main");
                    dbManager.update(1,"stored", emailET.getText().toString(), "student");
            }
            else{
                ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
            }
            Log.e("Message", singleTransformer.token);

        }catch(Exception e){

        }
    }
    @Subscribe
    public void onResponse(Auth.LoginTeacherResponse loginResponse){
        try{
            SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
            if (singleTransformer.status){
                UserData.insertTeacherDetails(singleTransformer.data);
                UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
                activity.startMainActivity("main");
                dbManager.update(1,"stored", emailET.getText().toString(), "teacher");
            }
            else{
                ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            }
            Log.e("Message", singleTransformer.token);

        }catch(Exception e){

        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
           selected = adapterView.getItemAtPosition(i).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @OnClick(R.id.loginBTN)
    void loginBTN(){
        if(selected.equals("Student")){
            if(!emailET.getText().toString().trim().isEmpty() && !passET.getText().toString().trim().isEmpty())
            Auth.getDefault().loginStudent(getContext(), emailET.getText().toString(), passET.getText().toString());
            else{
                emailET.setError("Fields must not empty!");
                passET.setError("Fields must not empty!");
            }
        }
        else{
            if(!emailET.getText().toString().trim().isEmpty() && !passET.getText().toString().trim().isEmpty())
             Auth.getDefault().loginTeacher(getContext(), emailET.getText().toString(), passET.getText().toString());
            else{
                emailET.setError("Fields must not empty!");
                passET.setError("Fields must not empty!");
            }
        }
    }

    @OnClick(R.id.signUpBTN)
    void signUpBTN(){
        activity.startRegisterActivity("first_screen");
    }
}
