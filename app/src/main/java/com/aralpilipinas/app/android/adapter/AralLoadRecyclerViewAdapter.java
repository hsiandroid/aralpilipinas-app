package com.aralpilipinas.app.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.data.model.api.LoadModel;
import com.aralpilipinas.app.data.model.api.SampleModel;
import com.aralpilipinas.app.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AralLoadRecyclerViewAdapter extends BaseRecylerViewAdapter<AralLoadRecyclerViewAdapter.ViewHolder, LoadModel>{

    private ClickListener clickListener;
    private int currentID=-1;
    private int prevID;
    private int newID;

    public AralLoadRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_aral_load));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));

//        holder.nameTXT.setText("Position " + holder.getItem().id);
        holder.loadTitleTXT.setText(holder.getItem().load_name);
        holder.loadIconIMG.setImageResource(holder.getItem().load_image);
        holder.adapterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                   currentID = position;
                   notifyDataSetChanged();
            }
        });
        if(currentID  == position){
            holder.loadTitleTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.lochmara));
            holder.loadIconIMG.setVisibility(View.GONE);
            holder.loadFilledIconIMG.setVisibility(View.VISIBLE);
        }
        else {
            holder.loadTitleTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.prelude));
            holder.loadIconIMG.setVisibility(View.VISIBLE);
            holder.loadFilledIconIMG.setVisibility(View.GONE);
        }
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder {

        @BindView(R.id.loadTitleTXT)     TextView loadTitleTXT;
        @BindView(R.id.loadIconIMG)      ImageView loadIconIMG;
        @BindView(R.id.loadFilledIconIMG)      ImageView loadFilledIconIMG;
        @BindView(R.id.adapterLayout ) View adapterLayout;


        public ViewHolder(View view) {
            super(view);
        }

        public LoadModel getItem() {
            return (LoadModel) super.getItem();
        }


    }
    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(LoadModel loadModel);
    }
    public void transitionViews(){

    }
} 
