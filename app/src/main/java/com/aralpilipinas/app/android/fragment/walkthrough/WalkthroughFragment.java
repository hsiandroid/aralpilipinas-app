package com.aralpilipinas.app.android.fragment.walkthrough;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.Html;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LandingActivity;
import com.aralpilipinas.app.android.activity.LoginActivity;
import com.aralpilipinas.app.android.adapter.SliderAdapter;
import com.aralpilipinas.app.android.fragment.sqlite.DBManager;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Objects;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class WalkthroughFragment extends BaseFragment {
    public static final String TAG = WalkthroughFragment.class.getName().toString();

    private ViewPager viewPager;
    private SliderAdapter sliderAdapter;
    private LandingActivity activity;
    private TextView [] mDots;
    private DBManager dbManager;


    @BindView(R.id.sliderPager)
    ViewPager sliderPager;
    @BindView(R.id.dotsLayout)
    LinearLayout dotsLayout;

    public static WalkthroughFragment newInstance() {
        WalkthroughFragment fragment = new WalkthroughFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_walkthrough;
    }

    @Override
    public void onViewReady() {
        activity = (LandingActivity) getContext();
        sliderAdapter = new SliderAdapter(activity);
        sliderPager.setAdapter(sliderAdapter);
        dotsIndicator(0);
        sliderPager.addOnPageChangeListener(viewlistener);
        dbManager = new DBManager(activity);
        dbManager.open();

    }

    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse) {
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    public void  dotsIndicator(int position){
        mDots = new TextView[4];
        dotsLayout.removeAllViews();
          for(int i = 0 ; i < mDots.length; i++){

              mDots[i] = new TextView(activity);
              mDots[i].setText(Html.fromHtml("&#8226"));
              mDots[i].setTextSize(60);
              mDots[i].setTextColor(getResources().getColor(R.color.colorPrimary));
              dotsLayout.addView(mDots[i]);
          }
          if(mDots.length >0){
              mDots[position].setTextColor(getResources().getColor(R.color.lochmara));
          }
    }
   ViewPager.OnPageChangeListener viewlistener  = new ViewPager.OnPageChangeListener() {
       @Override
       public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

       }

       @Override
       public void onPageSelected(int position) {
               dotsIndicator(position);
               if(position == Objects.requireNonNull(sliderPager.getAdapter()).getCount()-1){
                   activity.startLoginActivity("startup");
                   dbManager.insertOnboard("stored");
               }
       }

       @Override
       public void onPageScrollStateChanged(int state) {

       }
   };


}
