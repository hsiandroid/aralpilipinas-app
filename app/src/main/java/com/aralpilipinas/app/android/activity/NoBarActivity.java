package com.aralpilipinas.app.android.activity;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.fragment.DefaultFragment;
import com.aralpilipinas.app.android.fragment.load.SuccessFragment;
import com.aralpilipinas.app.android.fragment.load.TutorialFragment;
import com.aralpilipinas.app.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class NoBarActivity extends RouteActivity {
    public static final String TAG = NoBarActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_nobar;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "tutorial":
                openTutorialFragment();
                break;
            case "success":
                openSuccessFragment();
                break;
        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openTutorialFragment(){ switchFragment(TutorialFragment.newInstance()); }
    public void openSuccessFragment(){ switchFragment(SuccessFragment.newInstance()); }
}
