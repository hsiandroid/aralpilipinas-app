package com.aralpilipinas.app.android.fragment.landing;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LandingActivity;
import com.aralpilipinas.app.android.activity.LoginActivity;
import com.aralpilipinas.app.android.dialog.ChangeUserDialog;
import com.aralpilipinas.app.android.fragment.sqlite.DBManager;
import com.aralpilipinas.app.data.model.api.HandlerModel;
import com.aralpilipinas.app.data.model.api.UserModel;
import com.aralpilipinas.app.data.preference.UserData;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.android.java.ToastMessage;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;
import com.aralpilipinas.app.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class PinLockFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = PinLockFragment.class.getName().toString();

    private LoginActivity activity;
    private DBManager dbManager;
    List<HandlerModel> handlerModelList;
    private UserData userData;

    boolean ispin1TXTFocus = false;
    boolean ispin2TXTFocus = false;
    boolean ispin3TXTFocus = false;
    boolean ispin4TXTFocus = false;

    @BindView(R.id.fillIn1TXT) ImageView fillIn1TXT;
    @BindView(R.id.fillIn2TXT) ImageView fillIn2TXT;
    @BindView(R.id.fillIn3TXT) ImageView fillIn3TXT;
    @BindView(R.id.fillIn4TXT) ImageView fillIn4TXT;

    @BindView(R.id.num0TXT) TextView num0TXT;
    @BindView(R.id.num1TXT) TextView num1TXT;
    @BindView(R.id.num2TXT) TextView num2TXT;
    @BindView(R.id.num3TXT) TextView num3TXT;
    @BindView(R.id.num4TXT) TextView num4TXT;
    @BindView(R.id.num5TXT) TextView num5TXT;
    @BindView(R.id.num6TXT) TextView num6TXT;
    @BindView(R.id.num7TXT) TextView num7TXT;
    @BindView(R.id.num8TXT) TextView num8TXT;
    @BindView(R.id.num9TXT) TextView num9TXT;
    @BindView(R.id.viewLayout) View viewLayout;
    @BindView(R.id.wrongPinTXT) TextView wrongPinTXT;
    @BindView(R.id.deleteIMG) ImageView deleteIMG;

    String pin1="", pin2="", pin3="", pin4="";
    String email="", type="";

    public static PinLockFragment newInstance() {
        PinLockFragment fragment = new PinLockFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_pin;
    }

    @Override
    public void onViewReady() {
        activity = (LoginActivity) getContext();
        userData = new UserData();
        ispin1TXTFocus = true;
        setListener();
        dbManager = new DBManager(activity);
        dbManager.open();
        getData();
    }
    @Override
    public void onResume() {
        super.onResume();
    }
    private void getData() {
        handlerModelList = dbManager.getAllData();
        for (HandlerModel handlerModel : handlerModelList) {
            email = handlerModel.getEmail();
            type = handlerModel.getType();
        }

        UserData.insert(UserData.USER_EMAIL, email);

    }

    private void checkPasscode() {
        String passcode = pin1 + pin2 + pin3 + pin4;
        if(passcode.length() == 4){
            if(type.equals("student")){
                Auth.getDefault().loginViaPinStudent(getContext(), email, passcode);
            }
            else if(type.equals("teacher")){
                Auth.getDefault().loginViaPinTeacher(getContext(), email, passcode);
            }
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse) {
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }


    private void setListener() {

        num0TXT.setOnClickListener(this);
        num1TXT.setOnClickListener(this);
        num2TXT.setOnClickListener(this);
        num3TXT.setOnClickListener(this);
        num4TXT.setOnClickListener(this);
        num5TXT.setOnClickListener(this);
        num6TXT.setOnClickListener(this);
        num7TXT.setOnClickListener(this);
        num8TXT.setOnClickListener(this);
        num9TXT.setOnClickListener(this);
        deleteIMG.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.num0TXT:
                if(ispin1TXTFocus) {
                    fillIn1TXT.setImageResource(R.drawable.bg_circle_fill);
                    checkFocus(fillIn1TXT);
                    pin1 = "0";
                }
                else if(ispin2TXTFocus){
                    fillIn2TXT.setImageResource(R.drawable.bg_circle_fill);
                    checkFocus(fillIn2TXT);
                    pin2 = "0";
                }
                 else if(ispin3TXTFocus){
                    fillIn3TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn3TXT);
                    pin3 = "0";
                }
                 else if(ispin4TXTFocus) {
                    fillIn4TXT.setImageResource(R.drawable.bg_circle_white);
                    pin4 = "0";
                    checkPasscode();
                }

                break;
            case R.id.num1TXT:
                if(ispin1TXTFocus){
                    fillIn1TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn1TXT);
                    pin1 = "1";
                }  else if(ispin2TXTFocus) {
                    fillIn2TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn2TXT);
                    pin2 = "1";
                } else if (ispin3TXTFocus) {
                    fillIn3TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn3TXT);
                    pin3 = "1";
                } else if (ispin4TXTFocus) {
                    fillIn4TXT.setImageResource(R.drawable.bg_circle_white);
                    pin4 = "1";
                    checkPasscode();
                }
                break;
            case R.id.num2TXT:
                if(ispin1TXTFocus){
                    fillIn1TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn1TXT);
                    pin1 = "2";
                } else if (ispin2TXTFocus) {
                    fillIn2TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn2TXT);
                    pin2 = "2";
                } else if (ispin3TXTFocus) {
                    fillIn3TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn3TXT);
                    pin3 = "2";
                } else if (ispin4TXTFocus) {
                    fillIn4TXT.setImageResource(R.drawable.bg_circle_white);
                    pin4 = "2";
                    checkPasscode();
                }
                break;
            case R.id.num3TXT:
                if (ispin1TXTFocus) {
                    fillIn1TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn1TXT);
                    pin1 = "3";
                } else if (ispin2TXTFocus) {
                    fillIn2TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn2TXT);
                    pin2 = "3";
                } else if (ispin3TXTFocus) {
                    fillIn3TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn3TXT);
                    pin3 = "3";
                } else if (ispin4TXTFocus) {
                    fillIn4TXT.setImageResource(R.drawable.bg_circle_white);
                    pin4 = "3";
                    checkPasscode();
                }
                break;
            case R.id.num4TXT:
                if (ispin1TXTFocus) {
                    fillIn1TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn1TXT);
                    pin1 = "4";
                } else if (ispin2TXTFocus) {
                    fillIn2TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn2TXT);
                    pin2 = "4";
                } else if (ispin3TXTFocus) {
                    fillIn3TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn3TXT);
                    pin3 = "4";
                } else if (ispin4TXTFocus) {
                    fillIn4TXT.setImageResource(R.drawable.bg_circle_white);
                    pin4 = "4";
                    checkPasscode();
                }
                break;
            case R.id.num5TXT:
                if (ispin1TXTFocus) {
                    fillIn1TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn1TXT);
                    pin1 = "5";
                } else if (ispin2TXTFocus) {
                    fillIn2TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn2TXT);
                    pin2 = "5";
                } else if (ispin3TXTFocus) {
                    fillIn3TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn3TXT);
                    pin3 = "5";
                } else if (ispin4TXTFocus) {
                    fillIn4TXT.setImageResource(R.drawable.bg_circle_white);
                    pin4 = "5";
                    checkPasscode();
                }
                break;
            case R.id.num6TXT:
                if (ispin1TXTFocus) {
                    fillIn1TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn1TXT);
                    pin1 = "6";
                } else if (ispin2TXTFocus) {
                    fillIn2TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn2TXT);
                    pin2 = "6";
                } else if (ispin3TXTFocus) {
                    fillIn3TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn3TXT);
                    pin3 = "6";
                } else if (ispin4TXTFocus) {
                    fillIn4TXT.setImageResource(R.drawable.bg_circle_white);
                    pin4 = "6";
                    checkPasscode();
                }
                break;
            case R.id.num7TXT:
                if (ispin1TXTFocus) {
                    fillIn1TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn1TXT);
                    pin1 = "7";
                } else if (ispin2TXTFocus) {
                    fillIn2TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn2TXT);
                    pin2 = "7";
                } else if (ispin3TXTFocus) {
                    fillIn3TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn3TXT);
                    pin3 = "7";
                } else if (ispin4TXTFocus) {
                    fillIn4TXT.setImageResource(R.drawable.bg_circle_white);
                    pin4 = "7";
                    checkPasscode();
                }
                break;
            case R.id.num8TXT:
                if (ispin1TXTFocus) {
                    fillIn1TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn1TXT);
                    pin1 = "8";
                } else if (ispin2TXTFocus) {
                    fillIn2TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn2TXT);
                    pin2 = "8";
                } else if (ispin3TXTFocus) {
                    fillIn3TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn3TXT);
                    pin3 = "8";
                } else if (ispin4TXTFocus) {
                    fillIn4TXT.setImageResource(R.drawable.bg_circle_white);
                    pin4 = "9";
                    checkPasscode();
                }
                break;
            case R.id.num9TXT:
                if (ispin1TXTFocus) {
                    fillIn1TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn1TXT);
                    pin1 = "9";
                } else if (ispin2TXTFocus) {
                    fillIn2TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn2TXT);
                    pin2 = "9";
                } else if (ispin3TXTFocus) {
                    fillIn3TXT.setImageResource(R.drawable.bg_circle_white);
                    checkFocus(fillIn3TXT);
                    pin3 = "9";
                } else if (ispin4TXTFocus) {
                    fillIn4TXT.setImageResource(R.drawable.bg_circle_white);
                    pin4 = "9";
                    checkPasscode();
                }
                break;
            case R.id.deleteIMG:
                if(fillIn4TXT.getDrawable() != null){
                    fillIn4TXT.setImageDrawable(null);
                    checkFocus(fillIn2TXT);
                }
                else if(fillIn3TXT.getDrawable() != null){
                    fillIn3TXT.setImageDrawable(null);
                    checkFocus(fillIn1TXT);
                }
                else if(fillIn2TXT.getDrawable() != null){
                    fillIn2TXT.setImageDrawable(null);
                    ispin1TXTFocus = true;
                    ispin2TXTFocus = false;
                    ispin3TXTFocus = false;
                    ispin4TXTFocus = false;
                }
                else if(fillIn1TXT.getDrawable() != null){
                    fillIn1TXT.setImageDrawable(null);
                    ispin1TXTFocus = true;
                    ispin2TXTFocus = false;
                    ispin3TXTFocus = false;
                    ispin4TXTFocus = false;
                    deleteIMG.setVisibility(View.INVISIBLE);
                }
                break;
        }

    }

    public void checkFocus(ImageView v){
        if(v.getId() == R.id.fillIn1TXT){
            if(v.getDrawable() != null) {
                ispin1TXTFocus = false;
                ispin2TXTFocus = true;
                ispin3TXTFocus = false;
                ispin4TXTFocus = false;
                deleteIMG.setVisibility(View.VISIBLE);
            }
        }
        else if(v.getId() == R.id.fillIn2TXT){
            if(v.getDrawable() != null) {
                ispin1TXTFocus = false;
                ispin2TXTFocus = false;
                ispin3TXTFocus = true;
                ispin4TXTFocus = false;
                deleteIMG.setVisibility(View.VISIBLE);
            }
        }
        else if(v.getId() == R.id.fillIn3TXT){
            if(v.getDrawable() != null) {
                ispin1TXTFocus = false;
                ispin2TXTFocus = false;
                ispin3TXTFocus = false;
                ispin4TXTFocus = true;
                deleteIMG.setVisibility(View.VISIBLE);
            }
        }

    }

    @OnClick({R.id.emailBTN})
    void loginBTN(){
        activity.startLoadActivity("change_user");

    }

    @Subscribe
    public void onResponse(Auth.LoginStudentResponse loginResponse){
        try{
            SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
            if (singleTransformer.status){
                UserData.insertStudentDetails(singleTransformer.data);
                UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
                ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
                activity.startMainActivity("main");
            }
            else{
                Animation shake = AnimationUtils.loadAnimation(activity, R.anim.shake_animation);
                viewLayout.startAnimation(shake);
                fillIn1TXT.setImageDrawable(null);
                fillIn2TXT.setImageDrawable(null);
                fillIn3TXT.setImageDrawable(null);
                fillIn4TXT.setImageDrawable(null);
                fillIn1TXT.requestFocus();
                ispin1TXTFocus = true;
                wrongPinTXT.setVisibility(View.VISIBLE);
                deleteIMG.setVisibility(View.INVISIBLE);
            }
            Log.e("Message", singleTransformer.token);

        }catch(Exception e){

        }
    }
    @Subscribe
    public void onResponse(Auth.LoginTeacherResponse loginResponse){
        try{
            SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
            if (singleTransformer.status){
                UserData.insertTeacherDetails(singleTransformer.data);
                UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
                ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
                activity.startMainActivity("main");
            }
            else{
                Animation shake = AnimationUtils.loadAnimation(activity, R.anim.shake_animation);
                viewLayout.startAnimation(shake);
                fillIn1TXT.setImageDrawable(null);
                fillIn2TXT.setImageDrawable(null);
                fillIn3TXT.setImageDrawable(null);
                fillIn4TXT.setImageDrawable(null);
                deleteIMG.setVisibility(View.INVISIBLE);
            }
            Log.e("Message", singleTransformer.token);

        }catch(Exception e){

        }
    }

}
