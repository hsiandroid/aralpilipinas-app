package com.aralpilipinas.app.android.fragment.pager;

import android.util.Log;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.adapter.AralLoadRecyclerViewAdapter;
import com.aralpilipinas.app.android.adapter.RegularLoadRecyclerViewAdapter;
import com.aralpilipinas.app.data.model.api.LoadModel;
import com.aralpilipinas.app.data.model.api.RegLoadModel;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SmartFragment extends BaseFragment {
    public static final String TAG = SmartFragment.class.getName().toString();
    private RegularLoadRecyclerViewAdapter regularLoadRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;


    @BindView(R.id.smartRecycler)  RecyclerView smartRecycler;

    public static SmartFragment newInstance() {
        SmartFragment fragment = new SmartFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_smart;
    }

    @Override
    public void onViewReady() {
        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        regularLoadRecyclerViewAdapter = new RegularLoadRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        smartRecycler.setLayoutManager(linearLayoutManager);
        smartRecycler.setAdapter(regularLoadRecyclerViewAdapter);
        regularLoadRecyclerViewAdapter.setNewData(dummyData());
    }
    private List<LoadModel> dummyData() {
        List<LoadModel> loadModels = new ArrayList<>();
        String [] price = {"399", "499"};
        int [] image = {R.drawable.ic_399,R.drawable.ic_499};
        String [] bytes = {"6gb", "8gb"};
        for(int i=0; i<image.length; i++){
            LoadModel loadModel = new LoadModel();
            loadModel.load_name = "Smart GIGA Video " + price[i];
            loadModel.load_type = "Available to Smart Prepaid Only";
            loadModel.load_details = bytes[i] + " + 1GB Everyday to Youtube, Iflix, Netflix";
            loadModel.load_image = image[i];
            loadModels.add(loadModel);
        }
        return  loadModels;
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpRecyclerView();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
}
