package com.aralpilipinas.app.android.activity;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.fragment.DefaultFragment;
import com.aralpilipinas.app.android.fragment.main.ConnectionTypeFragment;
import com.aralpilipinas.app.android.fragment.profile.ConnectFragment;
import com.aralpilipinas.app.android.fragment.profile.ProfileFragment;
import com.aralpilipinas.app.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ProfileActivity extends RouteActivity {
    public static final String TAG = ProfileActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_profile;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "profile":
                openProfileFragment();
                break;
            case "connect":
                openConnectFragment();
                break;
            case "plan":
                openPlanFragment();
                break;

        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openProfileFragment() {
        switchFragment(ProfileFragment.newInstance());
    }
    public void openConnectFragment() {
        switchFragment(ConnectFragment.newInstance());
    }
    public void openPlanFragment() {
        switchFragment(ConnectionTypeFragment.newInstance());
    }

}
