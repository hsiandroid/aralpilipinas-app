package com.aralpilipinas.app.android.fragment.startup;

import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LandingActivity;
import com.aralpilipinas.app.android.activity.LoginActivity;
import com.aralpilipinas.app.android.dialog.DefaultDialog;
import com.aralpilipinas.app.android.fragment.sqlite.DBManager;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class StartUpFragment extends BaseFragment  {
    public static final String TAG = StartUpFragment.class.getName().toString();
    private LoginActivity activity;


    public static StartUpFragment newInstance() {
        StartUpFragment fragment = new StartUpFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_startup;
    }

    @Override
    public void onViewReady() {
        activity = (LoginActivity) getContext();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @OnClick(R.id.loginBTN)
    void loginBTN(){
       activity.startLoginActivity("login");
    }
    @OnClick(R.id.signUpBTN)
    void signUpBTN(){
        activity.startRegisterActivity("first_screen");
    }

}
