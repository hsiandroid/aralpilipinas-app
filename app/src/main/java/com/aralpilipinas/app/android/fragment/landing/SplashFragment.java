package com.aralpilipinas.app.android.fragment.landing;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.widget.Toast;


import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LandingActivity;
import com.aralpilipinas.app.android.activity.LoginActivity;
import com.aralpilipinas.app.android.fragment.sqlite.DBManager;
import com.aralpilipinas.app.data.model.api.UserModel;
import com.aralpilipinas.app.data.preference.UserData;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.android.java.Log;
import com.aralpilipinas.app.vendor.android.java.ToastMessage;
import com.aralpilipinas.app.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SplashFragment extends BaseFragment {
    public static final String TAG = SplashFragment.class.getName().toString();

    private DBManager dbManager;
    private LandingActivity activity;
    private Runnable runnable;
    private Handler handler;
    private UserData userData;


    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();
        return fragment;
    }

    @Override
    public void onViewReady() {
        activity  = (LandingActivity) getContext();
        userData = new UserData();
        dbManager = new DBManager(activity);
        dbManager.open();
    }


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_splash;}

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isNetworkAvailable()){
                checkLogin();
        }else{
            openMainFragment();
        }
    }
        public void openMainFragment() {
            activity = (LandingActivity) getActivity();
        runnable = new Runnable() {
            @Override
            public void run() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        activity.startMainActivity("main");
                        if (UserData.isLogin()){
                            activity.startMainActivity("main");
                        } else {
                            activity.startMainActivity("main");
                        }
                    }
                });
            }
        };

        handler = new Handler();
            handler.postDelayed(runnable, 3000);

    }
    public void openLoginFragment() {
        activity = (LandingActivity) getActivity();
        runnable = new Runnable() {
            @Override
            public void run() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        activity.startLoginActivity("pinlock");
                    }
                });
            }
        };

        handler = new Handler();
        handler.postDelayed(runnable, 3000);

    }
    private void  checkLogin(){
        if(UserData.getUserModel().type != null && UserData.getUserModel().type.equals("student")) {
            Auth.getDefault().checkStudentLogin(getContext());
        }
        else if(UserData.getUserModel().type != null &&  UserData.getUserModel().type.equals("teacher")) {
            Auth.getDefault().checkTeacherLogin(getContext());
        }
        else {
            openLoginFragment();
        }
    }


    private void refreshToken() {
        if(UserData.getUserModel().type != null &&  UserData.getUserModel().type.equals("student")) {
            Auth.getDefault().refreshStudentToken(getContext());
        }  else if(UserData.getUserModel().type != null && UserData.getUserModel().type.equals("teacher")) {
            Auth.getDefault().refreshTeacherToken(getContext());
        }
        else {
            openLoginFragment();
        }
    }

    @Subscribe
    public void onResponse(Auth.CheckStudentResponse response){
        SingleTransformer<UserModel> userTransformer = response.getData(SingleTransformer.class);
        if(userTransformer.status){
                UserData.insertStudentDetails(userTransformer.data);
                 activity.startMainActivity("main");
                ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.SUCCESS);
        }else {
            refreshToken();
        }
    }
    @Subscribe
    public void onResponse(Auth.CheckTeacherResponse response){
        SingleTransformer<UserModel> userTransformer = response.getData(SingleTransformer.class);
        if(userTransformer.status){
                UserData.insertTeacherDetails(userTransformer.data);
                 activity.startMainActivity("main");
                ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.SUCCESS);
        }else {
            refreshToken();
        }
    }

    @Subscribe
    public void onResponse(Auth.RefreshStudentTokenResponse response){
        SingleTransformer<UserModel> userTransformer = response.getData(SingleTransformer.class);
        if(userTransformer.status){
                UserData.insertStudentDetails(userTransformer.data);
                UserData.insert(UserData.AUTHORIZATION, userTransformer.token);
                ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.SUCCESS);
              activity.startMainActivity("main");
        }else {
            openLoginFragment();
        }
    }
    @Subscribe
    public void onResponse(Auth.RefreshTeacherTokenResponse response){
        SingleTransformer<UserModel> userTransformer = response.getData(SingleTransformer.class);
        if(userTransformer.status){
                UserData.insertTeacherDetails(userTransformer.data);
               UserData.insert(UserData.AUTHORIZATION, userTransformer.token);
                ToastMessage.show(getContext(), userTransformer.msg, ToastMessage.Status.SUCCESS);
               activity.startMainActivity("main");
        }else {
            openLoginFragment();
        }
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
