package com.aralpilipinas.app.android.activity;


import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.fragment.DefaultFragment;
import com.aralpilipinas.app.android.fragment.landing.LoginFragment;
import com.aralpilipinas.app.android.fragment.walkthrough.WalkthroughFragment;
import com.aralpilipinas.app.android.route.RouteActivity;
import com.aralpilipinas.app.data.preference.UserData;
import com.aralpilipinas.app.vendor.android.java.Log;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class WalkthroughActivity extends RouteActivity {
    public static final String TAG = WalkthroughActivity.class.getName().toString();
    private WalkthroughActivity activity;
    private UserData userData;
    String onBoard="";

    @Override
    public int onLayoutSet() {
        return R.layout.activity_walkthrough;
    }

    @Override
    public void onViewReady() {
        activity = (WalkthroughActivity) getContext();

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "login":
                break;
            default:

                break;
        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openWalkthroughFragment(){ switchFragment(WalkthroughFragment.newInstance()); }

    @Override
    public void onResume() {
        super.onResume();

    }
}
