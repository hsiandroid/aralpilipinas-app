package com.aralpilipinas.app.android.fragment.load;

import android.util.Log;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LoadActivity;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class PaymentConfirmationFragment extends BaseFragment {
    public static final String TAG = PaymentConfirmationFragment.class.getName().toString();
    private LoadActivity activity;

    public static PaymentConfirmationFragment newInstance() {
        PaymentConfirmationFragment fragment = new PaymentConfirmationFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_payment_confirmation;
    }

    @Override
    public void onViewReady() {
        activity = (LoadActivity) getContext();
        activity.setTitle("PAYMENT CONFIRMATION");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
    @OnClick(R.id.payBTN)
    void payBTN(){
        activity.startNoBarActivity("success");
    }
}
