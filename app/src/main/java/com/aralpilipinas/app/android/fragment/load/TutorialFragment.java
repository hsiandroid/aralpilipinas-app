package com.aralpilipinas.app.android.fragment.load;

import android.util.Log;
import android.widget.TextView;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LoadActivity;
import com.aralpilipinas.app.android.activity.NoBarActivity;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class TutorialFragment extends BaseFragment {
    public static final String TAG = TutorialFragment.class.getName().toString();
    private NoBarActivity activity;

    public static TutorialFragment newInstance() {
        TutorialFragment fragment = new TutorialFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_load_tutorial;
    }

    @Override
    public void onViewReady() {
        activity = (NoBarActivity) getContext();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @OnClick(R.id.nextBTN)
    void nextBTN(){
       activity.startLoadActivity("buy_load");
    }
}
