package com.aralpilipinas.app.android.activity;

import android.widget.ListView;


import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.adapter.DrawerAdapter;
import com.aralpilipinas.app.android.fragment.DefaultFragment;
import com.aralpilipinas.app.android.fragment.main.MainFragment;
import com.aralpilipinas.app.android.route.RouteActivity;
import com.aralpilipinas.app.data.model.api.NavDrawerModel;
import com.aralpilipinas.app.vendor.android.java.Keyboard;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MainActivity extends RouteActivity {
    public static final String TAG = MainActivity.class.getName().toString();
    private MainActivity activity;



    @Override
    public int onLayoutSet() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewReady() {

        activity = (MainActivity) getContext();

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName) {
            case "main":
                openMainFragment();
                break;

            default:
                openDefaultFragment();
                break;
        }
    }

    public void openDefaultFragment() {
        switchFragment(DefaultFragment.newInstance());
    }

    public void openMainFragment() {
        switchFragment(MainFragment.newInstance());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


}
