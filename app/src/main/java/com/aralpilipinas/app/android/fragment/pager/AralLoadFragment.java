package com.aralpilipinas.app.android.fragment.pager;

import android.util.Log;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LoadActivity;
import com.aralpilipinas.app.android.adapter.AralLoadRecyclerViewAdapter;
import com.aralpilipinas.app.data.model.api.LoadModel;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AralLoadFragment extends BaseFragment {
    public static final String TAG = AralLoadFragment.class.getName().toString();
    private AralLoadRecyclerViewAdapter aralLoadRecyclerViewAdapter;
    private LinearLayoutManager linearLayout;
    private LoadActivity activity;

    @BindView(R.id.aralLoadRecycler)  RecyclerView aralLoadRecycler;

    public static AralLoadFragment newInstance() {
        AralLoadFragment fragment = new AralLoadFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_aral_load;
    }

    @Override
    public void onViewReady() {
        activity = (LoadActivity) getContext();
        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        aralLoadRecyclerViewAdapter = new AralLoadRecyclerViewAdapter(getContext());
        linearLayout = new LinearLayoutManager(getContext());
        aralLoadRecycler.setLayoutManager(linearLayout);
        aralLoadRecycler.setAdapter(aralLoadRecyclerViewAdapter);
        aralLoadRecyclerViewAdapter.setNewData(dummyData());
    }

    private List<LoadModel> dummyData() {
        List<LoadModel> loadModels = new ArrayList<>();
        String [] aralLoad = {"20", "50", "99", "129", "199"};
       for(int i=0; i<aralLoad.length; i++){
           LoadModel loadModel = new LoadModel();
           loadModel.load_name = "Aral Load " + aralLoad[i];
           loadModel.load_image = R.drawable.favicon;
           loadModels.add(loadModel);
       }
       return  loadModels;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

}
