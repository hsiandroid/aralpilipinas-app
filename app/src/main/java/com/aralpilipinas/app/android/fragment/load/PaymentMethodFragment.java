package com.aralpilipinas.app.android.fragment.load;

import android.util.Log;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LoadActivity;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class PaymentMethodFragment extends BaseFragment {
    public static final String TAG = PaymentMethodFragment.class.getName().toString();
    private LoadActivity activity;

    public static PaymentMethodFragment newInstance() {
        PaymentMethodFragment fragment = new PaymentMethodFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_payment_method;
    }

    @Override
    public void onViewReady() {
       activity = (LoadActivity) getContext();
        activity.setTitle("PAYMENT METHOD");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
    @OnClick(R.id.nextBTN)
    void nextBTN(){
        activity.openPaymentConfirmationFragment();
    }

}
