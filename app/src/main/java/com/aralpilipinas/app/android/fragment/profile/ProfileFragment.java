package com.aralpilipinas.app.android.fragment.profile;

import android.content.DialogInterface;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.ProfileActivity;
import com.aralpilipinas.app.android.adapter.DrawerAdapter;
import com.aralpilipinas.app.data.model.api.NavDrawerModel;
import com.aralpilipinas.app.data.model.api.UserModel;
import com.aralpilipinas.app.data.preference.UserData;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.android.java.Keyboard;
import com.aralpilipinas.app.vendor.android.java.ToastMessage;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;
import com.aralpilipinas.app.vendor.server.transformer.SingleTransformer;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ProfileFragment extends BaseFragment implements DrawerAdapter.ClickListener {
    public static final String TAG = ProfileFragment.class.getName().toString();
    private ProfileActivity activity;
    private UserData userData;
    private String type= "";
    private String name="";


    @BindView(R.id.nameTXT) TextView nameTXT;
    @BindView(R.id.userIDTXT) TextView userIDTXT;
    @BindView(R.id.schoolNameTXT) TextView schoolNameTXT;
    @BindView(R.id.profileIMG)  CircleImageView profileIMG;

    private DrawerAdapter drawerAdapter;
    @BindView(R.id.drawerLV)  ListView drawerLV;
    @BindView(R.id.drawer_layout) DrawerLayout drawer_layout;

    @BindView(R.id.nameNAVTXT) TextView nameNAVTXT;
    @BindView(R.id.descTXT) TextView descTXT;
    @BindView(R.id.profileNavIMG)  CircleImageView profileNavIMG;


    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onViewReady() {
        userData = new UserData();
        activity = (ProfileActivity) getContext();
        type = userData.getUserModel().type;
        schoolNameTXT.setText(userData.getUserModel().school_name);
        SetDrawer();
        checkUser();
        setImage();
    }

    private void setImage() {
        if (!UserData.getUserModel().avatar.filename.equals("")){
            Picasso.with(getContext()).load(UserData.getUserModel().avatar.thumbPath)
                    .error(R.drawable.avatar)
                    .placeholder(R.drawable.avatar)
                    .into(profileIMG);
            Picasso.with(getContext()).load(UserData.getUserModel().avatar.thumbPath)
                    .error(R.drawable.avatar)
                    .placeholder(R.drawable.avatar)
                    .into(profileNavIMG);

        }  else {
            Picasso.with(getContext()).load(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .placeholder(R.drawable.avatar)
                    .into(profileIMG);
            Picasso.with(getContext()).load(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .placeholder(R.drawable.avatar)
                    .into(profileNavIMG);
        }
    }


    private void checkUser() {
        if (type != null && type.equals("student")) {
            userIDTXT.setText(userData.getUserModel().student_id);
            nameTXT.setText(userData.getUserModel().lastname + ", " + userData.getUserModel().firstname);
            nameNAVTXT.setText(userData.getUserModel().firstname + " " + userData.getUserModel().lastname);
            descTXT.setText(userData.getUserModel().contactNumber);
            Auth.getDefault().studentProfile(getContext());
        }
        else if(type !=null && type.equals("teacher")){
                userIDTXT.setText(userData.getUserModel().teacher_id);
            nameTXT.setText(userData.getUserModel().lastname + ", " + userData.getUserModel().firstname);
            nameNAVTXT.setText(userData.getUserModel().firstname + " " + userData.getUserModel().lastname);
            descTXT.setText(userData.getUserModel().contactNumber);
            Auth.getDefault().teacherProfile(getContext());
        }


    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        type = userData.getUserModel().type;
        setImage();
        checkUser();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @OnClick(R.id.planBTN)
    void planBTN(){
      activity.startProfileActivity("plan");
    }


  @OnClick(R.id.editProfileBTN)
    void editProfileBTN(){
      activity.startLoadActivity("update_profile");
  }


    public void setSelectedItem(String item) {
        drawerAdapter.setSelectedItem(item);
    }

    private void SetDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(getActivity(), drawer_layout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_layout.setDrawerListener(toggle);
        toggle.syncState();

        drawerAdapter = new DrawerAdapter(getContext());
        drawerAdapter.setClickListener(this);
        drawerAdapter.setNewData(NavDrawer());
        drawerLV.setAdapter(drawerAdapter);
    }


    private List<NavDrawerModel> NavDrawer() {
        List<NavDrawerModel> navDrawerListModels = new ArrayList<>();

        NavDrawerModel navDrawerModel = new NavDrawerModel();
        navDrawerModel.id = 1;
        navDrawerModel.item = "Update Profile";
        navDrawerListModels.add(navDrawerModel);

        navDrawerModel = new NavDrawerModel();
        navDrawerModel.id = 2;
        navDrawerModel.item = "Change Password";
        navDrawerListModels.add(navDrawerModel);
        navDrawerModel = new NavDrawerModel();
        navDrawerModel.id = 3;
        navDrawerModel.item = "Log out";
        navDrawerListModels.add(navDrawerModel);

        return navDrawerListModels;
    }

    public void drawer() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            drawer_layout.openDrawer(GravityCompat.START);
        }
    }

    @OnClick(R.id.menuBTN)
    void menuBTNOnClicked() {
        drawer();
    }


    public void logOut(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        if(type !=null && type.equals("student")){
                            Auth.getDefault().logoutStudent(activity);
                        }
                        else if(type !=null && type.equals("teacher")){
                            Auth.getDefault().logoutTeacher(activity);
                        }

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        dialog.dismiss();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Are you sure to logout?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();

    }

    @Override
    public void onItemClick(NavDrawerModel navDrawerModel) {
        drawer_layout.closeDrawer(GravityCompat.START);
        switch (navDrawerModel.id) {
            case 1:
                activity.startLoadActivity("update_profile");
                Keyboard.hideKeyboard(activity);
                break;
            case 2:
                activity.startLoadActivity("change_password");
                Keyboard.hideKeyboard(activity);
                break;
            case 3:
                logOut();
                Keyboard.hideKeyboard(activity);
                break;
        }
    }
    @Subscribe
    public void onResponse(Auth.LogoutResponse logoutResponse){
        BaseTransformer baseTransformer = logoutResponse.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            UserData.clearData();
            activity.startLoginActivity("pinlock");
        }else{
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
        }
        Log.e("Message", logoutResponse.getData(BaseTransformer.class).msg);
    }
    @Subscribe
    public void onResponse(Auth.TeacherProfileResponse response){
        try{
            SingleTransformer<UserModel> singleTransformer = response.getData(SingleTransformer.class);
            if (singleTransformer.status){
                UserData.insertTeacherDetails(singleTransformer.data);
                setImage();
            }
            else{
                ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
            }
            Log.e("Message", singleTransformer.token);

        }catch(Exception e){

        }
    }
    @Subscribe
    public void onResponse(Auth.StudentProfileResponse response){
        try{
            SingleTransformer<UserModel> singleTransformer = response.getData(SingleTransformer.class);
            if (singleTransformer.status){
                UserData.insertStudentDetails(singleTransformer.data);
                setImage();
            }
            else{
                ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
            }
            Log.e("Message", singleTransformer.token);

        }catch(Exception e){

        }
    }


}
