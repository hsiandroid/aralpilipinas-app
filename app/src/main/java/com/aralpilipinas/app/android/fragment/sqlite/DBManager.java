package com.aralpilipinas.app.android.fragment.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.aralpilipinas.app.data.model.api.HandlerModel;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

public class DBManager {
    private DatabaseHelper dbHelper;
    private Context context;
    private SQLiteDatabase database;


    public DBManager(Context c) {
        context = c;
    }

    public DBManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }
    public Cursor fetch() {
        String[] columns = new String[] { DatabaseHelper._ID, DatabaseHelper.STORED , DatabaseHelper.EMAIL, DatabaseHelper.TYPE};
        Cursor cursor = database.query(DatabaseHelper.TABLE_ONBOARD, columns, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }
    public void close() {
        dbHelper.close();
    }
    public void insertOnboard(String stored) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelper.STORED, stored);
        database.insert(DatabaseHelper.TABLE_ONBOARD, null, contentValue);
    }
    public void insertUserType(String email, String type) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelper.EMAIL, email);
        contentValue.put(DatabaseHelper.TYPE, type);
        database.insert(DatabaseHelper.TABLE_USER, null, contentValue);
    }

    public String Stored(long _id){
        String stored = "not stored";
        String whereclause = "_ID=?";
        String[] whereargs = new String[]{String.valueOf(_id)};
        Cursor csr = database.query(DatabaseHelper.TABLE_ONBOARD, null, whereclause, whereargs, null, null,null);
        if (csr != null && csr.getCount()>0) {
            csr.moveToFirst();
            stored = csr.getString(csr.getColumnIndex(DatabaseHelper.STORED));
        }
        return  stored;
    }

    public int update(long _id, String stored, String email, String type) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.STORED, stored);
        contentValues.put(DatabaseHelper.EMAIL, email);
        contentValues.put(DatabaseHelper.TYPE, type);
        int i = database.update(DatabaseHelper.TABLE_ONBOARD, contentValues, DatabaseHelper._ID + " = " + _id, null);
        return i;
    }

    public List<HandlerModel> getAllData() {
        List<HandlerModel> list = new ArrayList<HandlerModel>();
        String selectQuery = "SELECT  * FROM " + DatabaseHelper.TABLE_ONBOARD;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HandlerModel handlerModel = new HandlerModel();
                handlerModel.setEmail(cursor.getString(2));
                handlerModel.setType(cursor.getString(3));
                list.add(handlerModel);
            } while (cursor.moveToNext());
        }

        return list;
    }
    public boolean exist(long _id, String Email){
        String email = "";
        String whereclause = "_ID=?";
        String[] whereargs = new String[]{String.valueOf(_id)};
        Cursor csr = database.query(DatabaseHelper.TABLE_ONBOARD, null, whereclause, whereargs, null, null,null);
        if (csr != null && csr.getCount()>0) {
            csr.moveToFirst();
            email = csr.getString(csr.getColumnIndex(DatabaseHelper.EMAIL));
            if(email.equals(Email)){
                return true;
            }
        }
        return  false;
    }
}
