package com.aralpilipinas.app.android.activity;

import android.view.View;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.fragment.DefaultFragment;
import com.aralpilipinas.app.android.fragment.landing.ChangeUserFragment;
import com.aralpilipinas.app.android.fragment.load.BuyLoadFragment;
import com.aralpilipinas.app.android.fragment.load.PaymentConfirmationFragment;
import com.aralpilipinas.app.android.fragment.load.PaymentMethodFragment;
import com.aralpilipinas.app.android.fragment.load.SelectLoadFragment;
import com.aralpilipinas.app.android.fragment.load.TutorialFragment;
import com.aralpilipinas.app.android.fragment.profile.ChangePasswordFragment;
import com.aralpilipinas.app.android.fragment.profile.UpdateProfileFragment;
import com.aralpilipinas.app.android.route.RouteActivity;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LoadActivity extends RouteActivity {
    public static final String TAG = LoadActivity.class.getName().toString();
    @BindView(R.id.loadHeader)  View loadHeader;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_load;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "buy_load":
                openBuyLoadFragment();
                break;
            case "update_profile":
                openUpdateProfileFragment();
                break;
            case "change_password":
                openChangePasswordFragment();
                break;
            case "change_user":
                openChangeUserFragment();
                break;
        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }

    public void openBuyLoadFragment(){ switchFragment(BuyLoadFragment.newInstance());}
    public void openSelectLoadFragment(){ switchFragment(SelectLoadFragment.newInstance());}
    public void openPaymentMethodFragment(){ switchFragment(PaymentMethodFragment.newInstance());}
    public void openPaymentConfirmationFragment(){ switchFragment(PaymentConfirmationFragment.newInstance());}
    public void openUpdateProfileFragment(){ switchFragment(UpdateProfileFragment.newInstance());}
    public void openChangePasswordFragment(){ switchFragment(ChangePasswordFragment.newInstance());}
    public void openChangeUserFragment(){ switchFragment(ChangeUserFragment.newInstance());}

}
