package com.aralpilipinas.app.android.fragment.usage;

import android.util.Log;
import android.widget.TextView;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.UsageActivity;
import com.aralpilipinas.app.data.preference.UserData;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class UsageReportFragment extends BaseFragment {
    public static final String TAG = UsageReportFragment.class.getName().toString();
    private UsageActivity activity;
    private UserData userData;

    @BindView(R.id.nameTXT) TextView nameTXT;
    @BindView(R.id.userIDTXT) TextView userIDTXT;

    public static UsageReportFragment newInstance() {
        UsageReportFragment fragment = new UsageReportFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_usage_report;
    }

    @Override
    public void onViewReady() {
        activity = (UsageActivity) getContext();
        nameTXT.setText(userData.getUserModel().lastname + ", " + userData.getUserModel().firstname);
        checkUser();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    private void checkUser() {
        if(userData.getUserModel().type.equals("student")){
            userIDTXT.setText(userData.getUserModel().student_id);
        }
        else if(userData.getUserModel().type.equals("teacher")){
            userIDTXT.setText(userData.getUserModel().teacher_id);
        }
    }
}
