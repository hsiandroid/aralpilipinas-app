package com.aralpilipinas.app.android.fragment.main;

import android.util.Log;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ConnectionTypeFragment extends BaseFragment {
    public static final String TAG = ConnectionTypeFragment.class.getName().toString();

    public static ConnectionTypeFragment newInstance() {
        ConnectionTypeFragment fragment = new ConnectionTypeFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_connection_type;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
}
