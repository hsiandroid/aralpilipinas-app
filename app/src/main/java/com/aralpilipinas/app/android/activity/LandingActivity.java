package com.aralpilipinas.app.android.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.fragment.landing.LoginFragment;
import com.aralpilipinas.app.android.fragment.landing.PinLockFragment;
import com.aralpilipinas.app.android.fragment.landing.SplashFragment;
import com.aralpilipinas.app.android.fragment.register.SignUpFragment;
import com.aralpilipinas.app.android.fragment.sqlite.DBManager;
import com.aralpilipinas.app.android.fragment.startup.StartUpFragment;
import com.aralpilipinas.app.android.fragment.walkthrough.WalkthroughFragment;
import com.aralpilipinas.app.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LandingActivity extends RouteActivity {
    public static final String TAG = LandingActivity.class.getName().toString();
    private  LandingActivity activity;
    private DBManager dbManager;
    public static String data = "";

    @Override
    public int onLayoutSet() {
        return R.layout.activity_landing;
    }

    @Override
    public void onViewReady() {
         activity = (LandingActivity) getContext();

         dbManager = new DBManager(activity);
         dbManager.open();
         data = dbManager.Stored(1);
         if(data.equals("stored")){
           initialFragment(TAG, "splash");
//           activity.startLoginActivity("splash");
         }
         else{
            initialFragment(TAG, "walkthrough");
        }
    }



    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "splash":
                openSplashFragment();
                break;
            case "walkthrough":
                openWalkthroughFragment();
                break;

        }
    }

    public void openWalkthroughFragment(){ switchFragment(WalkthroughFragment.newInstance()); }
    public void openSplashFragment(){ switchFragment(SplashFragment.newInstance(),false); }


}
