package com.aralpilipinas.app.android.fragment.pager;

import android.util.Log;

import androidx.viewpager.widget.ViewPager;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.adapter.PagerAdapter;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;
import com.google.android.material.tabs.TabLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RegularLoadFragment extends BaseFragment {
    public static final String TAG = RegularLoadFragment.class.getName().toString();

    @BindView(R.id.tab_layout)  TabLayout tab_layout;
    @BindView(R.id.viewPager)   ViewPager viewPager;

    public static RegularLoadFragment newInstance() {
        RegularLoadFragment fragment = new RegularLoadFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_regular_load;
    }

    @Override
    public void onViewReady() {
//        pager();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    private void pager() {
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        adapter.AddFragment(new AralLoadFragment(), "SMART");
        adapter.AddFragment(new RegularLoadFragment(), "GLOBE");
        adapter.AddFragment(new RegularLoadFragment(), "TNT");
        adapter.AddFragment(new RegularLoadFragment(), "TM");
        viewPager.setAdapter(adapter);
        tab_layout.setupWithViewPager(viewPager);
    }
}
