package com.aralpilipinas.app.android.fragment.register;

import android.util.Log;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.RegisterActivity;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FirstRegisterFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener {
    public static final String TAG = FirstRegisterFragment.class.getName().toString();
    private RegisterActivity activity ;

    @BindView(R.id.radioGroup)  RadioGroup radioGroup;
    @BindView(R.id.rdoBTNSTUD)  RadioButton rdoBTNSTUD;
    @BindView(R.id.rdoBTNTEACH)  RadioButton rdoBTNTEACH;

    public static FirstRegisterFragment newInstance() {
        FirstRegisterFragment fragment = new FirstRegisterFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_first_reg;
    }

    @Override
    public void onViewReady() {
        activity = (RegisterActivity) getContext();
        radioGroup.setOnCheckedChangeListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if(R.id.rdoBTNSTUD == i){
            activity.openSecondFragment("student");
        }
        else if(R.id.rdoBTNTEACH == i){
            activity.openSecondFragment("teacher");
        }

    }
}
