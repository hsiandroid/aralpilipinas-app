package com.aralpilipinas.app.android.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.data.model.api.LoadModel;
import com.aralpilipinas.app.data.model.api.RegLoadModel;
import com.aralpilipinas.app.data.model.api.SampleModel;
import com.aralpilipinas.app.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RegularLoadRecyclerViewAdapter extends BaseRecylerViewAdapter<RegularLoadRecyclerViewAdapter.ViewHolder, LoadModel>{

    private ClickListener clickListener;
    private int currentID = -1;

    public RegularLoadRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_reg_load));
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));

//        holder.nameTXT.setText("Position " + holder.getItem().id);
        holder.planIMG.setImageResource(holder.getItem().load_image);
        holder.titleTXT.setText(holder.getItem().load_name);
        holder.loadTypeTXT.setText(holder.getItem().load_type);
        holder.detailsTXT.setText(holder.getItem().load_details);
        holder.adapterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentID = position;
                notifyDataSetChanged();
            }
        });
        if(currentID == position){
            holder.rdoIMG.setImageResource(R.drawable.bg_circle_lochamara);
        }
        else {
            holder.rdoIMG.setImageResource(R.drawable.bg_circle_fill);

        }

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

//        @BindView(R.id.nameTXT)     TextView nameTXT;
        @BindView(R.id.adapterLayout) View adapterLayout;
        @BindView(R.id.planIMG)        CircleImageView planIMG;
        @BindView(R.id.titleTXT)       TextView titleTXT;
        @BindView(R.id.loadTypeTXT)    TextView loadTypeTXT;
        @BindView(R.id.detailsTXT)     TextView detailsTXT;
        @BindView(R.id.rdoIMG)    CircleImageView rdoIMG;

        public ViewHolder(View view) {
            super(view);
        }

        public LoadModel getItem() {
            return (LoadModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(LoadModel loadModel);
    }
} 
