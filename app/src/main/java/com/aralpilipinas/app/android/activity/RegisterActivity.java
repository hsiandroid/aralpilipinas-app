package com.aralpilipinas.app.android.activity;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.fragment.DefaultFragment;
import com.aralpilipinas.app.android.fragment.register.FirstRegisterFragment;
import com.aralpilipinas.app.android.fragment.register.FourthRegisterFragment;
import com.aralpilipinas.app.android.fragment.register.SecondRegisterFragment;
import com.aralpilipinas.app.android.fragment.register.StartedFragment;
import com.aralpilipinas.app.android.fragment.register.ThirdRegisterFragment;
import com.aralpilipinas.app.android.route.RouteActivity;
import com.aralpilipinas.app.data.model.api.AccountModel;
import com.aralpilipinas.app.data.model.api.ValidationModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RegisterActivity extends RouteActivity {
    public static final String TAG = RegisterActivity.class.getName().toString();
    private RegisterActivity activity ;
    private AccountModel accountModels;
    private ValidationModel validationModels;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_register;
    }

    @Override
    public void onViewReady() {
    activity = (RegisterActivity) getContext();
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "first_screen":
                openFirstFragment();
                break;
            case "fourth_screen":
                openFourthFragment();
                break;
            default:
                break;
        }
    }

    public void openFirstFragment(){
        switchFragment(FirstRegisterFragment.newInstance());
    }
    public void openSecondFragment(String user_type){
        switchFragment(SecondRegisterFragment.newInstance(user_type));
    }
    public void openThirdFragment(){
        switchFragment(ThirdRegisterFragment.newInstance());
    }
    public void openFourthFragment(){
        switchFragment(FourthRegisterFragment.newInstance());
    }
    public void openStartedFragment(){
        switchFragment(StartedFragment.newInstance());
    }
    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }

    public void setAccountModel(AccountModel accountModel){
        this.accountModels = accountModel;
    }

    public AccountModel getAccountModel(){
        return accountModels;
    }

    public void setValidationModel(ValidationModel validationModel){
        this.validationModels = validationModel;
    }

    public ValidationModel getValidationModel(){
        return validationModels;
    }



}
