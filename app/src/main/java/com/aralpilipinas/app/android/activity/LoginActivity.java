package com.aralpilipinas.app.android.activity;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.fragment.DefaultFragment;
import com.aralpilipinas.app.android.fragment.landing.LoginFragment;
import com.aralpilipinas.app.android.fragment.landing.PinLockFragment;
import com.aralpilipinas.app.android.fragment.landing.SplashFragment;
import com.aralpilipinas.app.android.fragment.register.SignUpFragment;
import com.aralpilipinas.app.android.fragment.startup.StartUpFragment;
import com.aralpilipinas.app.android.fragment.walkthrough.WalkthroughFragment;
import com.aralpilipinas.app.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LoginActivity extends RouteActivity {
    public static final String TAG = LoginActivity.class.getName().toString();
    private LoginActivity activity;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_login;
    }

    @Override
    public void onViewReady() {
        activity = (LoginActivity) getContext();
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "login":
                openLoginFragment();
                break;
            case "startup":
                openStartUpFragment();
                break;
            case "pinlock":
                openPinLockFragment();
                break;
            case "onboard":
                openSplashFragment();
                break;
        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openLoginFragment(){
        switchFragment(LoginFragment.newInstance());
    }
    public void openSignUpFragment(){ switchFragment(SignUpFragment.newInstance()); }
    public void openStartUpFragment(){ switchFragment(StartUpFragment.newInstance()); }
    public void openWalkthroughFragment(){ switchFragment(WalkthroughFragment.newInstance()); }
    public void openPinLockFragment(){ switchFragment(PinLockFragment.newInstance()); }
    public void openSplashFragment(){ switchFragment(SplashFragment.newInstance(),false); }
}
