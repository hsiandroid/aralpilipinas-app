package com.aralpilipinas.app.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.data.model.api.SampleModel;
import com.aralpilipinas.app.vendor.android.base.BaseListViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DefaultListViewAdapter extends BaseListViewAdapter<DefaultListViewAdapter.ViewHolder, SampleModel> {

    private ClickListener clickListener;

    public DefaultListViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_default));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.nameTXT.setText("Position " + holder.getItem().id);
    }

    public class ViewHolder extends BaseListViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)     TextView nameTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public SampleModel getItem() {
            return (SampleModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(SampleModel sampleModel);
    }
} 
