package com.aralpilipinas.app.android.dialog;

import android.content.Context;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.vendor.android.base.BaseDialog;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ChangeUserDialog extends BaseDialog {
	public static final String TAG = ChangeUserDialog.class.getName().toString();
	private Context context;
	private  Callback callback;

	public static ChangeUserDialog newInstance(Context context, Callback callback) {
		ChangeUserDialog dialog = new ChangeUserDialog();
		dialog.context = context;
		dialog.callback = callback;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_default;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
	public interface Callback{
		void onSuccess();
	}
}
