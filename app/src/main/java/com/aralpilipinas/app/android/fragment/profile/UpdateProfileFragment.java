package com.aralpilipinas.app.android.fragment.profile;

import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.EditText;
import android.widget.Spinner;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LoadActivity;
import com.aralpilipinas.app.config.Keys;
import com.aralpilipinas.app.data.model.api.UserModel;
import com.aralpilipinas.app.data.preference.UserData;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.android.java.ImageManager;
import com.aralpilipinas.app.vendor.android.java.ToastMessage;
import com.aralpilipinas.app.vendor.server.request.APIRequest;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;
import com.aralpilipinas.app.vendor.server.transformer.SingleTransformer;
import com.aralpilipinas.app.vendor.server.util.ErrorResponseManger;
import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class UpdateProfileFragment extends BaseFragment implements BSImagePicker.OnSingleImageSelectedListener, ImageManager.Callback {
    public static final String TAG = UpdateProfileFragment.class.getName().toString();
    private LoadActivity activity;
    private String type= "";
    @BindView(R.id.profileIMG)    CircleImageView profileIMG;
    @BindView(R.id.fnameET)       EditText fnameET;
    @BindView(R.id.lnameET)       EditText lnameET;
    @BindView(R.id.emailET)       EditText emailET;
    @BindView(R.id.contactET)     EditText contactET;
    @BindView(R.id.birthDateET)    EditText birthDateET;
    @BindView(R.id.spinnerLevel)   Spinner spinnerLevel;



    public static UpdateProfileFragment newInstance() {
        UpdateProfileFragment fragment = new UpdateProfileFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_udpate_profile;
    }

    @Override
    public void onViewReady() {
        activity = (LoadActivity) getContext();
        type = UserData.getUserModel().type;
        activity.setTitle("My Profile");
        setDetails(UserData.getUserModel());
    }

    private void setDetails(UserModel userModel) {
        Glide.with(activity)
                .load(file)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar))
                .into(profileIMG);

        fnameET.setText(userModel.firstname);
        lnameET.setText(userModel.lastname);
        emailET.setText(userModel.email);
        contactET.setText(userModel.contactNumber);
        birthDateET.setText(userModel.birthdate);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @OnClick(R.id.profileIMG)
    void profileIMG(){
        openFileChooser();
    }
    @OnClick(R.id.uploadBTN)
    void uploadBTN(){
        openFileChooser();
    }

    private void openFileChooser() {
        BSImagePicker pickerDialog = new BSImagePicker.Builder("com.aralpilipinas.app.fileprovider").build();
        pickerDialog.show(getChildFragmentManager(), "picker");
    }


    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
            ImageManager.getFileFromBitmap(activity, bitmap, "image1", this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private File file;
    @Override
    public void success(File file) {
        this.file = file;
        if(type.equals("teacher")) {
            Auth.getDefault().updateTeacherAvatar(activity, file);
        }
        else if(type.equals("student")) {
            Auth.getDefault().updateStudentAvatar(activity, file);
        }
    }


    @Subscribe
    public void onResponse(Auth.UpdateAvatarResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status){
            Picasso.with(getContext()).load(file)
                    .error(R.drawable.avatar)
                    .placeholder(R.drawable.avatar)
                    .into(profileIMG);
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }


    @OnClick(R.id.saveBTN)
    void saveBTN(){
        if(type.equals("teacher")){
            APIRequest apiRequest = Auth.getDefault().updateTeacherProfile(getContext())
            .addParameter(Keys.FIRSTNAME,fnameET.getText().toString().trim())
            .addParameter(Keys.LASTNAME,lnameET.getText().toString().trim())
            .addParameter(Keys.EMAIL,emailET.getText().toString().trim())
            .addParameter(Keys.CONTACT_NUMBER,contactET.getText().toString().trim())
            .addParameter(Keys.BIRTHDATE,birthDateET.getText().toString().trim())
            .addParameter(Keys.SCHOOL_LEVEL, UserData.getUserModel().school_level);
            apiRequest.execute();
        }
        else if(type.equals("student")){
            APIRequest apiRequest = Auth.getDefault().updateStudentProfile(getContext())
                    .addParameter(Keys.FIRSTNAME,fnameET.getText().toString().trim())
                    .addParameter(Keys.LASTNAME,lnameET.getText().toString().trim())
                    .addParameter(Keys.EMAIL,emailET.getText().toString().trim())
                    .addParameter(Keys.CONTACT_NUMBER,contactET.getText().toString().trim())
                    .addParameter(Keys.BIRTHDATE,birthDateET.getText().toString().trim())
                    .addParameter(Keys.SCHOOL_LEVEL, UserData.getUserModel().school_level);
            apiRequest.execute();
        }
    }
    @Subscribe
    public void onResponse(Auth.UpdateProfileResponse response){
        SingleTransformer<UserModel> baseTransformer = response.getData(SingleTransformer.class);
        if (baseTransformer.status){
            activity.onBackPressed();
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
        } else {
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
            if (!ErrorResponseManger.first(baseTransformer.error.email).equals("")){
                emailET.setError(ErrorResponseManger.first(baseTransformer.error.email));
            }if (!ErrorResponseManger.first(baseTransformer.error.firstname).equals("")){
                fnameET.setError(ErrorResponseManger.first(baseTransformer.error.firstname));
            }if (!ErrorResponseManger.first(baseTransformer.error.lastname).equals("")){
                lnameET.setError(ErrorResponseManger.first(baseTransformer.error.lastname));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.contactNumber).equals("")){
                contactET.setError(ErrorResponseManger.first(baseTransformer.error.contactNumber));
            }if (!ErrorResponseManger.first(baseTransformer.error.birthdate).equals("")){
                birthDateET.setError(ErrorResponseManger.first(baseTransformer.error.birthdate));
            }
        }
    }
}
