package com.aralpilipinas.app.android.route;

import android.content.Intent;

import com.aralpilipinas.app.android.activity.LandingActivity;
import com.aralpilipinas.app.android.activity.LoadActivity;
import com.aralpilipinas.app.android.activity.LoginActivity;
import com.aralpilipinas.app.android.activity.MainActivity;
import com.aralpilipinas.app.android.activity.NoBarActivity;
import com.aralpilipinas.app.android.activity.ProfileActivity;
import com.aralpilipinas.app.android.activity.RegisterActivity;
import com.aralpilipinas.app.android.activity.UsageActivity;
import com.aralpilipinas.app.vendor.android.base.BaseActivity;
import com.aralpilipinas.app.vendor.android.base.RouteManager;

/**
 * Created by Labyalo on 2/6/2017.
 */

public class RouteActivity extends BaseActivity {

    public void startMainActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(MainActivity.class)
                .addActivityTag("main")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

    }

    public void startRegisterActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(RegisterActivity.class)
                .addActivityTag("register")
                .addFragmentTag(fragmentTAG)
                .startActivity();

    }
    public void startLoginActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(LoginActivity.class)
                .addActivityTag("login")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
    }
    public void startLandingActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(LandingActivity.class)
                .addActivityTag("landing")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                 finish();
    }

    public void startLoadActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(LoadActivity.class)
                .addActivityTag("load")
                .addFragmentTag(fragmentTAG)
                .startActivity();

    }
    public void startUsageActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(UsageActivity.class)
                .addActivityTag("usage")
                .addFragmentTag(fragmentTAG)
                .startActivity();

    }
    public void startNoBarActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(NoBarActivity.class)
                .addActivityTag("nobar")
                .addFragmentTag(fragmentTAG)
                .startActivity();

    }
    public void startProfileActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(ProfileActivity.class)
                .addActivityTag("profile")
                .addFragmentTag(fragmentTAG)
                .startActivity();

    }
//    @Subscribe
//    public void invalidToken(InvalidToken invalidToken){
//        Log.e("Token", "Expired");
//        EventBus.getDefault().unregister(this);
//        UserData.insert(new UserModel());
//        UserData.insert(UserData.TOKEN_EXPIRED, true);
//        startLandingActivity();
//    }
}
