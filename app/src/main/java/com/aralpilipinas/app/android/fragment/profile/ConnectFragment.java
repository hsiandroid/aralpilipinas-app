package com.aralpilipinas.app.android.fragment.profile;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.net.VpnService;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.ProfileActivity;
import com.aralpilipinas.app.android.adapter.DrawerAdapter;
import com.aralpilipinas.app.data.model.api.NavDrawerModel;
import com.aralpilipinas.app.data.model.api.UserModel;
import com.aralpilipinas.app.data.preference.UserData;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.server.request.VPN;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.android.java.CheckInternetConnection;
import com.aralpilipinas.app.vendor.android.java.Keyboard;
import com.aralpilipinas.app.vendor.android.java.ToastMessage;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;
import com.aralpilipinas.app.vendor.server.transformer.SingleTransformer;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.blinkt.openvpn.OpenVpnApi;
import de.blinkt.openvpn.core.OpenVPNService;
import de.blinkt.openvpn.core.OpenVPNThread;
import de.blinkt.openvpn.core.VpnStatus;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ConnectFragment extends BaseFragment implements DrawerAdapter.ClickListener {
    public static final String TAG = ConnectFragment.class.getName().toString();
    private UserData userData;
    String username="";
    String configuration="";
    private String type= "";

    public static ConnectFragment newInstance() {
        ConnectFragment fragment = new ConnectFragment();
        return fragment;
    }

    private CheckInternetConnection connection;
    private ProfileActivity activity;
    private OpenVPNThread vpnThread = new OpenVPNThread();
    private OpenVPNService vpnService = new OpenVPNService();
    boolean vpnStart = false;

    @BindView(R.id.statusTXT)                   TextView statusTXT;
    @BindView(R.id.connectBTN)                  TextView connectBTN;
    @BindView(R.id.durationTXT)                 TextView durationTXT;
    @BindView(R.id.infoCON)                     View infoCON;
    @BindView(R.id.lastPacketReceiveTXT)        TextView lastPacketReceiveTXT;
    @BindView(R.id.byteInTXT)                   TextView byteInTXT;
    @BindView(R.id.byteOutTXT)                  TextView byteOutTXT;

    private DrawerAdapter drawerAdapter;
    @BindView(R.id.drawerLV)                 ListView drawerLV;
    @BindView(R.id.drawer_layout)            DrawerLayout drawer_layout;

    @BindView(R.id.nameNAVTXT) TextView nameNAVTXT;
    @BindView(R.id.descTXT) TextView descTXT;
    @BindView(R.id.profileNavIMG)
    CircleImageView profileNavIMG;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_status;
    }

    @Override
    public void onViewReady() {
        activity = (ProfileActivity) getContext();
        connection = new CheckInternetConnection();
        userData = new UserData();
        username =  userData.getUserModel().username;
        type = userData.getUserModel().type;
        SetDrawer();
        checkUser();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter("connectionState"));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isServiceRunning();
        VpnStatus.initLogCache(getActivity().getCacheDir());
    }
    private void checkUser() {
        if(type !=null && type.equals("student")){
            VPN.getDefault().studentsCredsVPN(activity);
            nameNAVTXT.setText(userData.getUserModel().firstname + " " + userData.getUserModel().lastname);
            descTXT.setText(userData.getUserModel().contactNumber);
            setIMG();
        }
        else if(type !=null && type.equals("teacher")){
            VPN.getDefault().teachersCredsVPN(activity);
            nameNAVTXT.setText(userData.getUserModel().firstname + " " + userData.getUserModel().lastname);
            descTXT.setText(userData.getUserModel().contactNumber);
            setIMG();
        }
    }

    public void setIMG(){
        if (!UserData.getUserModel().avatar.filename.equals("")){
            Picasso.with(getContext()).load(UserData.getUserModel().avatar.thumbPath)
                    .error(R.drawable.avatar)
                    .placeholder(R.drawable.avatar)
                    .into(profileNavIMG);

        }  else {
            Picasso.with(getContext()).load(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .placeholder(R.drawable.avatar)
                    .into(profileNavIMG);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        username =  userData.getUserModel().username;
        type = userData.getUserModel().type;
        checkUser();

    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    public void setStatus(String connectionState) {
        if (connectionState!= null)
            switch (connectionState) {
                case "DISCONNECTED":
                    status("connect");
                    vpnStart = false;
                    vpnService.setDefaultStatus();
                    statusTXT.setText("NOT CONNECTED");
                    break;
                case "CONNECTED":
                    vpnStart = true;// it will use after restart this activity
                    status("connected");
                    statusTXT.setText("CONNECTED");
                    break;
                case "WAIT":
                    statusTXT.setText("waiting for server connection!!");
                    break;
                case "AUTH":
                    statusTXT.setText("server authenticating...");
                    break;
                case "RECONNECTING":
                    status("connecting");
                    statusTXT.setText("Reconnecting...");
                    break;
                case "NONETWORK":
                    statusTXT.setText("No network connection");
                    break;
            }

    }

    @SuppressLint("NewApi")
    public void status(String status) {

        switch (status){
            case "connect":
            case "tryDifferentServer":
            case "invalidDevice":
                connectBTN.setText("Connect");
                connectBTN.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.green)));
                infoCON.setVisibility(View.GONE);
                durationTXT.setVisibility(View.GONE);
                break;
            case "connecting":
                connectBTN.setText("Connecting");
                connectBTN.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.green)));
                infoCON.setVisibility(View.GONE);
                durationTXT.setVisibility(View.GONE);
                break;
            case "connected":
                connectBTN.setText("Disconnect");
                connectBTN.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.persian_red)));
                infoCON.setVisibility(View.VISIBLE);
                durationTXT.setVisibility(View.VISIBLE);
                break;
            case "loading":
                connectBTN.setText("Loading");
                infoCON.setVisibility(View.GONE);
                durationTXT.setVisibility(View.GONE);
                break;
            case "authenticationCheck":
                connectBTN.setText("Authentication...");
                connectBTN.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.green)));
                infoCON.setVisibility(View.GONE);
                durationTXT.setVisibility(View.GONE);
                break;
        }
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                setStatus(intent.getStringExtra("state"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                String duration = intent.getStringExtra("duration");
                String lastPacketReceive = intent.getStringExtra("lastPacketReceive");
                String byteIn = intent.getStringExtra("byteIn");
                String byteOut = intent.getStringExtra("byteOut");

                if (duration == null) duration = "00:00:00";
                if (lastPacketReceive == null) lastPacketReceive = "0";
                if (byteIn == null) byteIn = " ";
                if (byteOut == null) byteOut = " ";
                updateConnectionStatus(duration, lastPacketReceive, byteIn, byteOut);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    public void updateConnectionStatus(String duration,  String lastPacketReceive, String byteIn, String byteOut) {
        durationTXT.setText("Duration: " + duration);
        lastPacketReceiveTXT.setText("Packet Received: " + lastPacketReceive + " second ago");
        byteInTXT.setText("Bytes In: " + byteIn);
        byteOutTXT.setText("Bytes Out: " + byteOut);
    }

    public void isServiceRunning() {
        setStatus(vpnService.getStatus());
    }

    @OnClick(R.id.connectBTN)
    void onConnectBTN(){
        if (vpnStart) {
            confirmDisconnect();
        }else {
            prepareVpn();
        }
    }

    public void confirmDisconnect(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        stopVpn();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Would you like to cancel the current VPN Connection?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private void startVpn() {
        try {
            InputStream conf = getActivity().getAssets().open("client.ovpn");
            InputStreamReader isr = new InputStreamReader(conf);
            BufferedReader br = new BufferedReader(isr);

            String config = "";
            String line;

            while (true) {
                line = br.readLine();
                if (line == null) break;
                config += line + "\n";
            }

            br.readLine();
            OpenVpnApi.startVpn(getContext(),  configuration, "singapore", username, "openvpn");

            statusTXT.setText("Connecting...");
            vpnStart = true;

        } catch (IOException | RemoteException e) {
            e.printStackTrace();
        }

    }

    private void prepareVpn() {
        if (!vpnStart) {
            if (getInternetStatus()) {
                Intent intent = VpnService.prepare(getContext());

                if (intent != null) {
                    startActivityForResult(intent, 1);
                } else startVpn();

                status("connecting");

            } else {
                ToastMessage.show(getContext(),"you have no internet connection !!", ToastMessage.Status.FAILED);
            }

        } else if (stopVpn()) {
            ToastMessage.show(getContext(),"Disconnect Successfully", ToastMessage.Status.FAILED);
        }
    }

    public boolean stopVpn() {
        try {
            vpnThread.stop();
            status("connect");
            vpnStart = false;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean getInternetStatus() {
        return connection.netCheck(getContext());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            startVpn();
        } else {
            ToastMessage.show(getContext(),"Permission Deny !!", ToastMessage.Status.FAILED);
        }
    }
    @Subscribe
    public void onResponse(VPN.TeacherVPNResponse teacherVPNResponse){
        Log.e("Message", teacherVPNResponse.getData(SingleTransformer.class).msg);
        SingleTransformer<UserModel> singleTransformer = teacherVPNResponse.getData(SingleTransformer.class);
        if (singleTransformer.status){
            configuration = singleTransformer.data.opvpn;
        }
    }
    @Subscribe
    public void onResponse(VPN.StudentVPNResponse studentVPNResponse){
        Log.e("Message", studentVPNResponse.getData(SingleTransformer.class).msg);
        SingleTransformer<UserModel> singleTransformer = studentVPNResponse.getData(SingleTransformer.class);
        if (singleTransformer.status){
            configuration = singleTransformer.data.opvpn;
        }
    }

    public void setSelectedItem(String item) {
        drawerAdapter.setSelectedItem(item);
    }

    private void SetDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(getActivity(), drawer_layout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_layout.setDrawerListener(toggle);
        toggle.syncState();

        drawerAdapter = new DrawerAdapter(getContext());
        drawerAdapter.setClickListener(this);
        drawerAdapter.setNewData(NavDrawer());
        drawerLV.setAdapter(drawerAdapter);
    }


    private List<NavDrawerModel> NavDrawer() {
        List<NavDrawerModel> navDrawerListModels = new ArrayList<>();

        NavDrawerModel navDrawerModel = new NavDrawerModel();
        navDrawerModel.id = 1;
        navDrawerModel.item = "Update Profile";
        navDrawerListModels.add(navDrawerModel);

        navDrawerModel = new NavDrawerModel();
        navDrawerModel.id = 2;
        navDrawerModel.item = "Change Password";
        navDrawerListModels.add(navDrawerModel);
        navDrawerModel = new NavDrawerModel();
        navDrawerModel.id = 3;
        navDrawerModel.item = "Log out";
        navDrawerListModels.add(navDrawerModel);

        return navDrawerListModels;
    }

    public void drawer() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            drawer_layout.openDrawer(GravityCompat.START);
        }
    }

    @OnClick(R.id.menuBTN)
    void menuBTNOnClicked() {
        drawer();
    }


    public void logOut(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        if(type !=null && type.equals("student")){
                            Auth.getDefault().logoutStudent(activity);
                        }
                        else if(type !=null && type.equals("teacher")){
                            Auth.getDefault().logoutTeacher(activity);
                        }

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        dialog.dismiss();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Are you sure to logout?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();

    }

    @Override
    public void onItemClick(NavDrawerModel navDrawerModel) {
        drawer_layout.closeDrawer(GravityCompat.START);
        switch (navDrawerModel.id) {
            case 1:
                activity.startLoadActivity("update_profile");
                Keyboard.hideKeyboard(activity);
                break;
            case 2:
                activity.startLoadActivity("change_password");
                Keyboard.hideKeyboard(activity);
                break;
            case 3:
                logOut();
                Keyboard.hideKeyboard(activity);
                break;
        }
    }
    @Subscribe
    public void onResponse(Auth.LogoutResponse logoutResponse){
        BaseTransformer baseTransformer = logoutResponse.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            UserData.clearData();
            activity.startLoginActivity("pinlock");
        }else{
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
        }
        Log.e("Message", logoutResponse.getData(BaseTransformer.class).msg);
    }


}
