package com.aralpilipinas.app.android.dialog;

import android.content.Context;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.vendor.android.base.BaseDialog;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DefaultDialog extends BaseDialog {
	public static final String TAG = DefaultDialog.class.getName().toString();
	private Context context;
	private  Callback callback;

	public static DefaultDialog newInstance(Context context, Callback callback) {
		DefaultDialog dialog = new DefaultDialog();
		dialog.context = context;
		dialog.callback = callback;
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_default;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
	public interface Callback{
		void onSuccess();
	}
}
