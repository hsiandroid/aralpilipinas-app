package com.aralpilipinas.app.android.fragment.landing;

import android.util.Log;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LoadActivity;
import com.aralpilipinas.app.android.fragment.sqlite.DBManager;
import com.aralpilipinas.app.data.model.api.HandlerModel;
import com.aralpilipinas.app.data.preference.UserData;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ChangeUserFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener {
    public static final String TAG = ChangeUserFragment.class.getName().toString();
    private LoadActivity activity;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.emailET)   EditText emailET;
    String selected ="", email ="";
    private DBManager dbManager;


    public static ChangeUserFragment newInstance() {
        ChangeUserFragment fragment = new ChangeUserFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_user;
    }

    @Override
    public void onViewReady() {
        activity = (LoadActivity) getContext();
        activity.setTitle("Change User Info");
        dbManager = new DBManager(activity);
        dbManager.open();
        radioGroup.setOnCheckedChangeListener(this);
        email = UserData.getString(UserData.USER_EMAIL);
        emailET.setText(email);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if(R.id.rdoBTNSTUD == i){
            selected = "student";
            changeUserInfo();
        }
        else{
            selected = "teacher";
            changeUserInfo();
        }
    }

    void changeUserInfo(){
        dbManager.update(1,"stored", emailET.getText().toString(), selected);
        dbManager.close();
        activity.startLoginActivity("pinlock");
    }
}
