package com.aralpilipinas.app.android.fragment.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {
    // Table Name
    public static final String TABLE_ONBOARD = "ONBOARD";
    public static final String TABLE_USER = "USER";
    // Table columns
    public static final String _ID = "_id";
    public static final String STORED = "stored";
    public static final String EMAIL = "email";
    public static final String TYPE = "type";
    static final int DB_VERSION = 1;
    static final String DB_NAME = "ARAL_APP.DB";

    // Creating table query
    private static final String CREATE_TABLE_ONBOARD = "create table " + TABLE_ONBOARD + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + STORED + " TEXT NOT NULL, " +  EMAIL + " TEXT, " + TYPE + " TEXT);";
    // Creating table query
    private static final String CREATE_TABLE_USER = "create table " + TABLE_USER + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + EMAIL + " TEXT NOT NULL, " + TYPE + " TEXT NOT NULL );";

    public DatabaseHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_ONBOARD);
        sqLiteDatabase.execSQL(CREATE_TABLE_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ONBOARD);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        onCreate(sqLiteDatabase);
    }
}
