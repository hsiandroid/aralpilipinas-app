package com.aralpilipinas.app.android.fragment.profile;

import android.util.Log;
import android.widget.EditText;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LoadActivity;
import com.aralpilipinas.app.data.preference.UserData;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.android.java.ToastMessage;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;
import com.aralpilipinas.app.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ChangePasswordFragment extends BaseFragment {
    public static final String TAG = ChangePasswordFragment.class.getName().toString();
    private LoadActivity activity;

    @BindView(R.id.passET)  EditText passET;
    @BindView(R.id.cpassET)  EditText cpassET;
    @BindView(R.id.cpass1ET)  EditText cpass1ET;
    String type = "";

    public static ChangePasswordFragment newInstance() {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_change_password;
    }

    @Override
    public void onViewReady() {
        activity = (LoadActivity) getContext();
        activity.setTitle("Change Password");
        type = UserData.getUserModel().type;

    }


    @OnClick(R.id.updateBTN)
    void updateBTN() {
        if (type.equals("teacher")) {
            Auth.getDefault().teacherPass(activity, passET.getText().toString().trim()
                    , cpassET.getText().toString().trim()
                    , cpass1ET.getText().toString().trim());
        } else if ((type.equals("student"))) {
            Auth.getDefault().studentPass(activity, passET.getText().toString().trim()
                    , cpassET.getText().toString().trim()
                    , cpass1ET.getText().toString().trim());
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
    @Subscribe
    public void onResponse(Auth.UpdatePasswordResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status){
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            activity.onBackPressed();
        } else {
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
            if (!ErrorResponseManger.first(baseTransformer.error.current_password).equals("")){
                passET.setError(ErrorResponseManger.first(baseTransformer.error.current_password));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.password).equals("")){
                cpassET.setError(ErrorResponseManger.first(baseTransformer.error.password));
            }
            if (!ErrorResponseManger.first(baseTransformer.error.password_confirmation).equals("")){
                cpass1ET.setError(ErrorResponseManger.first(baseTransformer.error.password_confirmation));
            }

        }
    }
}

