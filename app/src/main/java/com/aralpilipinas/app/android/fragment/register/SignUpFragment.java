package com.aralpilipinas.app.android.fragment.register;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LandingActivity;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SignUpFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = SignUpFragment.class.getName().toString();

    private LandingActivity landingActivity;



    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    @BindView(R.id.signUpBTN)                       TextView signUpBTN;
    @BindView(R.id.signInBTN)                       TextView signInBTN;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_signup;
    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
        signInBTN.setOnClickListener(this);
        signUpBTN.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.signUpBTN:
                Toast.makeText(getContext(), "SIGN UP SUCCESS", Toast.LENGTH_LONG).show();
                break;
            case R.id.signInBTN:
                Toast.makeText(getContext(), "SIGN IN SUCCESS", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
