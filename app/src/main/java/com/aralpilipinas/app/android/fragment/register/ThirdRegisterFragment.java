package com.aralpilipinas.app.android.fragment.register;

import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.RegisterActivity;
import com.aralpilipinas.app.config.Keys;
import com.aralpilipinas.app.data.model.api.LoadModel;
import com.aralpilipinas.app.data.model.api.SchoolModel;
import com.aralpilipinas.app.data.model.api.ValidationModel;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.android.java.ToastMessage;
import com.aralpilipinas.app.vendor.server.request.APIRequest;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;
import com.aralpilipinas.app.vendor.server.transformer.CollectionTransformer;
import com.aralpilipinas.app.vendor.server.transformer.SingleTransformer;
import com.aralpilipinas.app.vendor.server.util.ErrorResponseManger;
import com.google.android.material.textfield.TextInputLayout;
import com.tiper.MaterialSpinner;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ThirdRegisterFragment extends BaseFragment implements MaterialSpinner.OnItemSelectedListener {
    public static final String TAG = ThirdRegisterFragment.class.getName().toString();
    private RegisterActivity activity ;
    private APIRequest apiRequest;
    @BindView(R.id.userNameTXT) TextView userNameTXT;
    @BindView(R.id.studentIdET) EditText studentIdET;
    @BindView(R.id.validationCodeET) EditText validationCodeET;
    @BindView(R.id.idType)  TextInputLayout inputLayout;

    @BindView(R.id.levelSpinner)       MaterialSpinner levelSpinner;
    @BindView(R.id.schoolNameSpinner)  MaterialSpinner schoolNameSpinner;
    String level, schoolName, schoolID;

    private ValidationModel validationModel;
    List<SchoolModel> schoolModels ;



    public static ThirdRegisterFragment newInstance() {
        ThirdRegisterFragment fragment = new ThirdRegisterFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_third_reg;
    }

    @Override
    public void onViewReady() {
        activity = (RegisterActivity) getContext();
        userNameTXT.setText("Hello , " + activity.getAccountModel().firstname + " " + activity.getAccountModel().lastname);
        populateSpinnerLevel();
        schoolModels = new ArrayList<>();
        levelSpinner.setOnItemSelectedListener(this);
        schoolNameSpinner.setOnItemSelectedListener(this);
        validationModel = new ValidationModel();
        filterUserID();
        Auth.getDefault().getRequestAll(activity);

    }

    private void filterUserID() {
        if(activity.getAccountModel().type.equals("student")){
            inputLayout.setHint("Student ID");
        }
        else {
            inputLayout.setHint("Teacher ID");
        }
    }

    private void populateSpinnerLevel() {
        List<String> level = new ArrayList<String>();
        String [] levels = {"Primary", "Secondary", "Tertiary"};
        for (int i = 0; i <levels.length; i++) {
            level.add(levels[i]);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, level);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        levelSpinner.setAdapter(dataAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Subscribe
    public void onResponse(Auth.PreRegister preRegisterResponse){
        BaseTransformer baseTransformer = preRegisterResponse.getData(BaseTransformer.class);
        if (baseTransformer.status){
            validationModel.type = activity.getAccountModel().type;
            validationModel.firstname = activity.getAccountModel().firstname;
            validationModel.lastname = activity.getAccountModel().lastname;
            validationModel.birthdate = activity.getAccountModel().birthdate;
            validationModel.username = activity.getAccountModel().username;
            validationModel.email = activity.getAccountModel().email;
            validationModel.contact_number = activity.getAccountModel().contactNumber;
            validationModel.address = activity.getAccountModel().address;
            validationModel.password = activity.getAccountModel().password;
            validationModel.password_confirmation = activity.getAccountModel().password_confirmation;
            validationModel.student_id = studentIdET.getText().toString().trim();
            validationModel.school_id = schoolID;
            validationModel.school_validation_code = validationCodeET.getText().toString().trim();
            validationModel.school_level = level;
            activity.setValidationModel(validationModel);
            activity.openFourthFragment();
        }
        else{
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);
            if(baseTransformer.error.studentId !=null ){
                studentIdET.setError(ErrorResponseManger.first(baseTransformer.error.studentId));
            }
            if(baseTransformer.error.schoolLevel != null ){
                levelSpinner.setError(ErrorResponseManger.first(baseTransformer.error.schoolLevel));
            }
            if(baseTransformer.error.schoolValidationCode != null){
                validationCodeET.setError(ErrorResponseManger.first(baseTransformer.error.schoolValidationCode));
            }

        }

    }

    @Subscribe
    public void onResponse(Auth.RequestSchoolResponse response){
        CollectionTransformer<SchoolModel> transformer = response.getData(SingleTransformer.class);
        if (transformer.status){
            setUpDetails(transformer.data);
        } else {
            ToastMessage.show(activity,transformer.msg, ToastMessage.Status.FAILED);
        }
    }

    public void setUpDetails(List<SchoolModel> data){
        List<String> ListofSchool = new ArrayList<String>();
              for(int i=0; i<data.size(); i++){
                  SchoolModel schoolModel = new SchoolModel();
                    schoolModel.id = data.get(i).id;
                    schoolModel.name = data.get(i).name;
                    schoolModel.address = data.get(i).address;
                    ListofSchool.add(data.get(i).name);
                    schoolModels.add(schoolModel);
              }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, ListofSchool);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        schoolNameSpinner.setAdapter(dataAdapter);
    }

    @OnClick(R.id.nextBTN)
    void nextBTN() {
        if(activity.getAccountModel().type.equals("student") ) {
            preRegStud();
        }
        else if(activity.getAccountModel().type.equals("teacher")){
            preRegTeach();
        }
        else{
            schoolNameSpinner.setError("Required Fields");
        }


    }


    @Override
    public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view, int i, long l) {
        switch (materialSpinner.getId()){
            case R.id.levelSpinner:
                level = levelSpinner.getSelectedItem().toString();
                break;
            case R.id.schoolNameSpinner:
                schoolName = schoolNameSpinner.getSelectedItem().toString();
                schoolID = String.valueOf(schoolNameSpinner.getSelectedItemId()+1);
                break;

        }

    }

    @Override
    public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {

    }

    public void preRegStud(){
        apiRequest = Auth.getDefault().preRegStudent(getContext())
                .addParameter(Keys.STUDENT_ID, studentIdET.getText().toString().trim())
                .addParameter(Keys.SCHOOL_LEVEL, level)
                .addParameter(Keys.SCHOOL_VALIDATION_CODE, validationCodeET.getText().toString().trim())
                .addParameter(Keys.SCHOOL_ID, schoolID)
                .addParameter(Keys.PROGRESS, "2");
        apiRequest.execute();


    }
    public void preRegTeach(){
        apiRequest = Auth.getDefault().preRegTeacher(getContext())
                .addParameter(Keys.TEACHER_ID, studentIdET.getText().toString().trim())
                .addParameter(Keys.SCHOOL_LEVEL, level)
                .addParameter(Keys.SCHOOL_VALIDATION_CODE, validationCodeET.getText().toString().trim())
                .addParameter(Keys.SCHOOL_ID, schoolID)
                .addParameter(Keys.PROGRESS, "2");
        apiRequest.execute();


    }



}
