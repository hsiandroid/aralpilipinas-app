package com.aralpilipinas.app.android.fragment.register;

import android.icu.util.Calendar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.RegisterActivity;
import com.aralpilipinas.app.config.Keys;
import com.aralpilipinas.app.data.model.api.AccountModel;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.android.java.ToastMessage;
import com.aralpilipinas.app.vendor.server.request.APIRequest;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;
import com.aralpilipinas.app.vendor.server.transformer.SingleTransformer;
import com.aralpilipinas.app.vendor.server.util.ErrorResponseManger;
import com.tiper.MaterialSpinner;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SecondRegisterFragment extends BaseFragment implements View.OnClickListener,  MaterialSpinner.OnItemSelectedListener {
    public static final String TAG = SecondRegisterFragment.class.getName().toString();
    private RegisterActivity activity ;
    private APIRequest apiRequest;
    @BindView(R.id.nxtBTN)
    TextView nxtBTN;
    @BindView(R.id.fnameET) EditText fnameET;
    @BindView(R.id.lnameET) EditText lnameET;
    @BindView(R.id.emailET) EditText emailET;
    @BindView(R.id.usernameET) EditText usernameET;
    @BindView(R.id.contactET) EditText contactET;
    @BindView(R.id.addressET) EditText addressET;
    @BindView(R.id.passwordET) EditText passwordET;
    @BindView(R.id.cPaswordET) EditText cPaswordET;
    @BindView(R.id.materialSpinnerMonth) MaterialSpinner materialSpinnerMonth;
    @BindView(R.id.materialSpinnerDay)   MaterialSpinner materialSpinnerDay;
    @BindView(R.id.materialSpinnerYear)  MaterialSpinner materialSpinnerYear;
    String month, date, year;
    String userType, firstname="", lastname="", contact="", email="", password="", address="",birth_date=" ", username="";


    public static SecondRegisterFragment newInstance(String type) {
        SecondRegisterFragment fragment = new SecondRegisterFragment();
        fragment.userType = type;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_second_reg;
    }

    @Override
    public void onViewReady() {
        activity = (RegisterActivity) getContext();
        nxtBTN.setOnClickListener(this);
        materialSpinnerMonth.setOnItemSelectedListener(this);
        materialSpinnerDay.setOnItemSelectedListener(this);
        materialSpinnerYear.setOnItemSelectedListener(this);
        populateSpinnerMonth();
        populateSpinnerDay();
        populateSpinnerYear();
    }


    private void populateSpinnerYear() {
        List<String> years = new ArrayList<String>();
        int thisYear = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            thisYear = Calendar.getInstance().get(Calendar.YEAR);
        }
        for (int i = 1900; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, years);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        materialSpinnerYear.setAdapter(dataAdapter);
    }

    private void populateSpinnerDay() {
        List<String> days = new ArrayList<String>();
        for (int i = 1; i <= 31; i++) {
            days.add(Integer.toString(i));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, days);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        materialSpinnerDay.setAdapter(dataAdapter);

    }

    private void populateSpinnerMonth() {
        List<String> months = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            months.add(Integer.toString(i));
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, months);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        materialSpinnerMonth.setAdapter(dataAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.PreRegister registerResponse){
        BaseTransformer baseTransformer = registerResponse.getData(BaseTransformer.class);
        if (baseTransformer.status){
            AccountModel accountModel = new AccountModel();
                       accountModel.type = userType;
                       accountModel.firstname = fnameET.getText().toString().trim();
                       accountModel.lastname = lnameET.getText().toString().trim();
                       accountModel.birthdate = birth_date;
                       accountModel.username = usernameET.getText().toString().trim();
                       accountModel.email = emailET.getText().toString().trim();
                       accountModel.address = addressET.getText().toString().trim();
                       accountModel.contactNumber = contactET.getText().toString().trim();
                       accountModel.password = passwordET.getText().toString().trim();
                       accountModel.password_confirmation = cPaswordET.getText().toString().trim();
                       activity.setAccountModel(accountModel);
                       activity.openThirdFragment();
        }
        else{
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);
            if(baseTransformer.error.firstname !=null ){
                fnameET.setError(ErrorResponseManger.first(baseTransformer.error.firstname));
            }
            if(baseTransformer.error.lastname != null ){
                lnameET.setError(ErrorResponseManger.first(baseTransformer.error.lastname));
            }
            if(baseTransformer.error.username!= null){
                usernameET.setError(ErrorResponseManger.first(baseTransformer.error.username));
            }
            if(baseTransformer.error.email != null){
                emailET.setError(ErrorResponseManger.first(baseTransformer.error.email));
            }
            if(baseTransformer.error.address !=null){
                addressET.setError(ErrorResponseManger.first(baseTransformer.error.address));
            }
            if( baseTransformer.error.contactNumber !=null){
                contactET.setError(ErrorResponseManger.first(baseTransformer.error.contactNumber));
            }
            if(baseTransformer.error.password !=null){
                passwordET.setError(ErrorResponseManger.first(baseTransformer.error.password));
                cPaswordET.setError(ErrorResponseManger.first(baseTransformer.error.password));
            }
            if(baseTransformer.error.birthdate !=null){
                materialSpinnerMonth.setError("Month is not Valid");
                materialSpinnerDay.setError("Day is not Valid");
                materialSpinnerYear.setError("Year is not Valid");
            }

        }
    }
    @Override
    public void onClick(View view) {
          switch (view.getId()){
              case R.id.nxtBTN:
                   if(isPasswordMatch(passwordET.getText().toString().trim(), cPaswordET.getText().toString().trim())) {
                        birth_date = year + "-" + month + "-" + date;
                         firstname = fnameET.getText().toString().trim();
                         lastname = lnameET.getText().toString().trim();
                         username =  usernameET.getText().toString().trim();
                         email =  emailET.getText().toString().trim();
                         contact = contactET.getText().toString().trim();
                         address = addressET.getText().toString().trim();
                         password = passwordET.getText().toString().trim();
                         if(userType.equals("student")) {
                             preRegStud();
                         }
                         else if(userType.equals("teacher")){
                             preRegTeach();

                         }
                   }
                   else
                      Toast.makeText(activity, "Password MisMatch", Toast.LENGTH_SHORT).show();

                  break;
          }
    }


    @Override
    public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view, int i, long l) {
              switch (materialSpinner.getId()){
                  case  R.id.materialSpinnerMonth:
                      month = materialSpinnerMonth.getSelectedItem().toString();
                      break;
                  case R.id.materialSpinnerDay:
                      date = materialSpinnerDay.getSelectedItem().toString();
                      break;
                  case R.id.materialSpinnerYear:
                      year = materialSpinnerYear.getSelectedItem().toString();
                      break;
                  default:

                      break;

              }
    }

    @Override
    public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {

    }


    public  boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean isPasswordMatch(String pass, String cpass){
           if(pass.equals(cpass)){
               return true;
           }
           return false;
    }
    public void preRegStud(){
        apiRequest = Auth.getDefault().preRegStudent(getContext())
                .addParameter(Keys.FIRSTNAME, firstname)
                .addParameter(Keys.LASTNAME, lastname)
                .addParameter(Keys.BIRTHDATE, birth_date)
                .addParameter(Keys.EMAIL, email)
                .addParameter(Keys.USERNAME, username)
                .addParameter(Keys.CONTACT_NUMBER, contact)
                .addParameter(Keys.ADDRESS, address)
                .addParameter(Keys.PASSWORD, password)
                .addParameter(Keys.PROGRESS, "1");
                apiRequest.execute();


    }
    private void preRegTeach() {
        apiRequest = Auth.getDefault().preRegTeacher(getContext())
                .addParameter(Keys.FIRSTNAME, firstname)
                .addParameter(Keys.LASTNAME, lastname)
                .addParameter(Keys.BIRTHDATE, birth_date)
                .addParameter(Keys.EMAIL, email)
                .addParameter(Keys.USERNAME, username)
                .addParameter(Keys.CONTACT_NUMBER, contact)
                .addParameter(Keys.ADDRESS, address)
                .addParameter(Keys.PASSWORD, password)
                .addParameter(Keys.PROGRESS, "1");
        apiRequest.execute();
    }



}
