package com.aralpilipinas.app.android.activity;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.fragment.DefaultFragment;
import com.aralpilipinas.app.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DefaultActivity extends RouteActivity {
    public static final String TAG = DefaultActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "login":
                break;
            default:
                openDefaultFragment();
                break;
        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
}
