package com.aralpilipinas.app.android.fragment.load;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.aralpilipinas.app.R;
import com.aralpilipinas.app.android.activity.LoadActivity;
import com.aralpilipinas.app.android.adapter.AralLoadRecyclerViewAdapter;
import com.aralpilipinas.app.android.adapter.PagerAdapter;
import com.aralpilipinas.app.android.fragment.pager.AralLoadFragment;
import com.aralpilipinas.app.android.fragment.pager.GlobeFragment;
import com.aralpilipinas.app.android.fragment.pager.RegularLoadFragment;
import com.aralpilipinas.app.android.fragment.pager.SmartFragment;
import com.aralpilipinas.app.android.fragment.pager.TMFragment;
import com.aralpilipinas.app.android.fragment.pager.TNTFragment;
import com.aralpilipinas.app.data.model.api.LoadModel;
import com.aralpilipinas.app.server.request.Auth;
import com.aralpilipinas.app.vendor.android.base.BaseFragment;
import com.aralpilipinas.app.vendor.server.transformer.BaseTransformer;
import com.google.android.material.tabs.TabLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SelectLoadFragment extends BaseFragment {
    public static final String TAG = SelectLoadFragment.class.getName().toString();
    private AralLoadRecyclerViewAdapter aralLoadRecyclerViewAdapter;
    private LinearLayoutManager linearLayout;
    private LoadActivity activity;

    @BindView(R.id.nextBTN)  TextView nextBTN;
    @BindView(R.id.tab_layout)  TabLayout tab_layout;
    @BindView(R.id.viewPager) ViewPager viewPager;
    @BindView(R.id.aralLoadRecycler)  RecyclerView aralLoadRecycler;
    @BindView(R.id.fragment_aral_load)  View  fragment_aral_load;
    @BindView(R.id.fragment_regular_load)  View  fragment_regular_load;
    @BindView(R.id.aralLoadBTN)  TextView aralLoadBTN;
    @BindView(R.id.regLoadBTN)  TextView regLoadBTN;


    public static SelectLoadFragment newInstance() {
        SelectLoadFragment fragment = new SelectLoadFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_select_load;
    }

    @Override
    public void onViewReady() {
        pager();
        setUpRecyclerView();
        activity = (LoadActivity) getContext();
        activity.setTitle("SELECT LOAD");
        aralLoadBTN.setBackgroundResource(R.drawable.bg_border_selected);
        aralLoadBTN.setTextColor(getResources().getColor(R.color.white));
        regLoadBTN.setBackgroundResource(R.drawable.bg_border_unselected);
        regLoadBTN.setTextColor(getResources().getColor(R.color.lochmara));
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        pager();
        setUpRecyclerView();

    }
    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    private void pager() {
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        adapter.AddFragment(new SmartFragment(), "SMART");
        adapter.AddFragment(new GlobeFragment(), "GLOBE");
        adapter.AddFragment(new TNTFragment(), "TNT");
        adapter.AddFragment(new TMFragment(), "TM");
        viewPager.setAdapter(adapter);
        tab_layout.setupWithViewPager(viewPager);
    }
    private void setUpRecyclerView() {
        aralLoadRecyclerViewAdapter = new AralLoadRecyclerViewAdapter(getContext());
        linearLayout = new LinearLayoutManager(getContext());
        aralLoadRecycler.setLayoutManager(linearLayout);
        aralLoadRecycler.setAdapter(aralLoadRecyclerViewAdapter);
        aralLoadRecyclerViewAdapter.setNewData(dummyData());
    }
    private List<LoadModel> dummyData() {
        List<LoadModel> loadModels = new ArrayList<>();
        String [] aralLoad = {"20", "50", "99", "129", "199"};
        for(int i=0; i<aralLoad.length; i++){
            LoadModel loadModel = new LoadModel();
            loadModel.load_name = "Aral Load " + aralLoad[i];
            loadModel.load_image = R.drawable.favicon;
            loadModels.add(loadModel);
        }
        return  loadModels;
    }

    @SuppressLint("ResourceAsColor")
    @OnClick(R.id.aralLoadBTN)
    void aralLoadBTN(){
        fragment_aral_load.setVisibility(View.VISIBLE);
        fragment_regular_load.setVisibility(View.GONE);
        regLoadBTN.setBackgroundResource(0);
        regLoadBTN.setBackgroundResource(R.drawable.bg_border_unselected);
        regLoadBTN.setTextColor(getResources().getColor(R.color.lochmara));
        aralLoadBTN.setBackgroundResource(R.drawable.bg_border_selected);
        aralLoadBTN.setTextColor(getResources().getColor(R.color.white));


    }
    @OnClick(R.id.regLoadBTN)
    void regLoadBTN(){
        pager();
        fragment_regular_load.setVisibility(View.VISIBLE);
        fragment_aral_load.setVisibility(View.GONE);
        aralLoadBTN.setBackgroundResource(0);
        aralLoadBTN.setBackgroundResource(R.drawable.bg_border_unselected);
        aralLoadBTN.setTextColor(getResources().getColor(R.color.lochmara));
        regLoadBTN.setBackgroundResource(R.drawable.bg_border_selected);
        regLoadBTN.setTextColor(getResources().getColor(R.color.white));
    }
    @OnClick(R.id.nextBTN)
    void setNextBTN(){
      activity.openPaymentMethodFragment();
    }
}
