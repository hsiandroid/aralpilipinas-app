package com.aralpilipinas.app.data.model.api;

import com.aralpilipinas.app.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ErrorModel extends AndroidModel {


    @SerializedName("firstname")
    public List<String> firstname;
    @SerializedName("lastname")
    public List<String> lastname;
    @SerializedName("birthdate")
    public List<String> birthdate;
    @SerializedName("email")
    public List<String> email;
    @SerializedName("contact_number")
    public List<String> contactNumber;
    @SerializedName("address")
    public List<String> address;
    @SerializedName("password")
    public List<String> password;
    @SerializedName("current_password")
    public List<String> current_password;
    @SerializedName("password_confirmation")
    public List<String> password_confirmation;
    @SerializedName("student_id")
    public List<String> studentId;
    @SerializedName("school_level")
    public List<String> schoolLevel;
    @SerializedName("school_validation_code")
    public List<String> schoolValidationCode;
    @SerializedName("school_id")
    public List<String> schoolId;
    @SerializedName("username")
    public List<String> username;


    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ErrorModel convertFromJson(String json) {
        return convertFromJson(json, ErrorModel.class);
    }

}
