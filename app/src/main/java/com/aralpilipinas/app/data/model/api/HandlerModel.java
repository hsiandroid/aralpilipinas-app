package com.aralpilipinas.app.data.model.api;

import com.aralpilipinas.app.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class HandlerModel extends AndroidModel {

    @SerializedName("name")
    public String name;
    public String email;
    public String type;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public HandlerModel convertFromJson(String json) {
        return convertFromJson(json, HandlerModel.class);
    }
}
