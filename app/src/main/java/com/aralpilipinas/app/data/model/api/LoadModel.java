package com.aralpilipinas.app.data.model.api;

import com.aralpilipinas.app.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LoadModel extends AndroidModel {

    @SerializedName("load_name")
    public String load_name;
    @SerializedName("load_image")
    public int  load_image;
    @SerializedName("load_details")
    public String load_details;
    @SerializedName("load_type")
    public String load_type;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public LoadModel convertFromJson(String json) {
        return convertFromJson(json, LoadModel.class);
    }
}
