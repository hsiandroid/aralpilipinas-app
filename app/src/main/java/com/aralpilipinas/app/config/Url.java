package com.aralpilipinas.app.config;

import com.aralpilipinas.app.vendor.android.java.security.SecurityLayer;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Url extends SecurityLayer {
    public static final String PRODUCTION_URL = decrypt("http://internal.aral-pilipinas.highlysucceed.com/");
    public static final String DEBUG_URL = decrypt("http://internal.aral-pilipinas.highlysucceed.com/");

    public static final String APP = App.production ? PRODUCTION_URL : DEBUG_URL;

    //login
    public static final String getLoginStudent(){return "/api/student/login.json";}
    public static final String getLoginTeacher(){return "/api/teacher/login.json";}


    //register
    public static final String getRegisterStudent(){return "/api/student/register.json";}
    public static final String getRegisterTeacher(){return "/api/teacher/register.json";}

    //logout
    public static final String getLogoutStudent() {return "/api/student/logout.json";}
    public static final String getLogoutTeacher() {return "/api/teacher/logout.json";}

    //list
    public static final String getListSchool(){return "/api/school.json";}

    //prereg
    public static final String getStudentPrereg(){return "/api/student/pre-register.json";}
    public static final String getTeacherPrereg(){return "/api/teacher/pre-register.json";}

    //opvpn
    public static final String getTeacherVPNCreds(){return "/api/teacher/profile/opvpn.json";}
    public static final String getStudentVPNCreds(){return "/api/student/profile/opvpn.json";}

    //checklogin
    public static final String getCheckStudLogin(){return "/api/student/check-login.json";}
    public static final String getCheckTeachLogin(){return "/api/teacher/check-login.json";}

   //refreshtoken
    public static final String getStudentToken(){return "/api/student/refresh-token.json";}
    public static final String getTeacherToken(){return "/api/teacher/refresh-token.json";}

    //avatar
    public static final String getTeacherAvatar(){return "/api/teacher/profile/edit-avatar.json";}
    public static final String getStudentAvatar(){return "/api/student/profile/edit-avatar.json";}

   //editprofile
    public static final String editTeacherProfile(){return "/api/teacher/profile/edit-profile.json";}
    public static final String editStudentProfile(){return "/api/student/profile/edit-profile.json";}

    //updatePassword
    public static final String updateTeacherPassword(){return "/api/teacher/profile/edit-password.json";}
    public static final String updateStudentPassword(){return "/api/student/profile/edit-password.json";}
    //loginViaPin
    public static final String loginTeacherViaPin(){return "/api/teacher/login-via-pin.json";}
    public static final String loginStudentViaPin(){return "/api/student/login-via-pin.json";}
    //profiles
    public static final String teacherProfileDetails(){return "/api/teacher/profile/show.json";}
    public static final String studentProfileDetails(){return "/api/student/profile/show.json";}

}

