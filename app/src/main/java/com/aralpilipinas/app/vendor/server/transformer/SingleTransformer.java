package com.aralpilipinas.app.vendor.server.transformer;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class SingleTransformer<T> extends BaseTransformer{

    @SerializedName("data")
    public T data;
}
